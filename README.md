# MultiLingo Translation App

Multilingo is a single page application that gives its users the ability to create, read and edit articles in different languages. There is public, private and administration part. The users can also rate and comment on articles in order to provide feedback to the article's author.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

## Built with:
[NestJS](https://nestjs.com/)  
[Angular](https://angular.io)

## Running the server

*** Before running the server you should create and include a .env file in the root folder. The file should look like that: ***

    PORT=3000
    DB_TYPE=mysql
    DB_HOST=localhost
    DB_PORT=3306
    DB_USERNAME=root
    DB_PASSWORD=123456
    DB_DATABASE_NAME=multilingodb
    JWT_SECRET=secretKey
    GOOGLE_APPLICATION_CREDENTIALS=src/config/google-service.json

```bash
    #Create a database schema with charset utf-8
```
```bash
    #Install all app dependencies
    $ npm i
```
```bash
    #Run database the seed
    $ npm run seed
```
```bash
    #Run the project in development mode
    $ npm run start

    #Run the project in watch mode
    $ npm run start:dev

    #Run the project in production mode
    $ npm run start:dev
```
```bash
    #Run tests
    $ npm test
```

## Running the client
```bash
    #Install all app dependencies
    $ npm i
```
```bash
    #Run the client in watch mode
    $ npm serve --open
```
```bash
    #Run tests
    $ ng test
```

## Preseeded users
To login use the following preseeded users

    - Admin
        username: admin
        password: 12345678

    - User 1
        username: pesho
        password: 12345678

    - User 2
        username: misho
        password: 12345678

## Contributors
[Martin Stoykov](https://gitlab.com/MStoykov46)  
[Ivan Keremidarski](https://gitlab.com/Vanko1)  
[Anton Ivanov](https://gitlab.com/santosbg)  


## License
To the extent possible under law, MIT has waived all copyright and related or neighboring rights to this work.

MIT © Multilingo 2019