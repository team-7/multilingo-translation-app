import { Expose, Transform } from 'class-transformer';
import { RoleE } from '../../../database/entities/role.entity';
import { ArticleE } from '../../../database/entities/article.entity';
import { SuppLangE } from '../../../database/entities/supp-lang.entity';

export class ShowUserDTO {

    @Expose()
    public id: string;

    @Expose()
    public username: string;

    @Expose()
    public name: string;

    @Expose()
    public surname: string;

    @Expose()
    public createdOn: Date;

    @Expose()
    public email: string;

    @Expose()
    public powerPoints: number;

    @Expose()
    public profileImg: string;

    @Expose()
    public isBanned: boolean;

    @Expose()
    public endOfBanDate: Date;

    @Expose()
    public banReason: string;

    @Expose()
    @Transform((_, obj) => obj.roles.map((role: RoleE) => role.name))
    public roles: string[];

    @Expose()
    public isDeleted: boolean;

    @Expose()
    @Transform((_, obj) => obj.prefLang.index)
    public prefLang: string;

    @Expose()
    @Transform((_, obj) => obj.editLanguages.map((lang: SuppLangE) => lang.name))
    public editLanguages: string[];

}
