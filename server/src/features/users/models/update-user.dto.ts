import { IsEmail, IsOptional, IsString, Length } from 'class-validator';
import { ApiModelProperty } from '@nestjs/swagger';

export class UpdateUserDTO {

    @ApiModelProperty({example: 'Ivan'})
    @IsOptional()
    @IsString()
    @Length(3, 20, {message: 'The name should be between 3 and 20 symbols!'})
    public name: string;

    @ApiModelProperty({example: 'Ivanov'})
    @IsOptional()
    @IsString()
    @Length(3, 20, {message: 'The surname should be between 3 and 20 symbols!'})
    public surname: string;

    @ApiModelProperty({example: 'ivan@ivanov.com'})
    @IsOptional()
    @IsEmail()
    @Length(5, 40, {message: 'The email should be between 5 and 40 symbols!'})
    public email: string;

    @ApiModelProperty({example: 'http://i.imgur.com/7wQ61EW.jpg'})
    @IsOptional()
    @IsString()
    public profileImg: string;

}
