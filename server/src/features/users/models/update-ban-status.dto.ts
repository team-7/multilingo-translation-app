import { ApiModelProperty } from '@nestjs/swagger';
import { IsString, Length, IsNumber } from 'class-validator';

export class UpdateBanStatusDTO {

    @ApiModelProperty({example: 'This one was just for fun'})
    @IsString()
    @Length(5, 40, {message: 'The ban reason should be between 5 and 40 symbols!'})
    public banReason: string;

    @ApiModelProperty({example: 3})
    @IsNumber()
    public days: number;

}
