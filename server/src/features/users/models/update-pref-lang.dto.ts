import { ApiModelProperty } from '@nestjs/swagger';
import { IsOptional, IsString } from 'class-validator';

export class UpdatePrefLangDTO {

    @ApiModelProperty({example: 'en'})
    @IsOptional()
    @IsString()
    public prefLang: string;

}
