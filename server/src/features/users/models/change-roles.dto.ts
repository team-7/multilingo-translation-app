import { ApiModelProperty } from '@nestjs/swagger';
import { IsOptional, IsString, Length, IsArray } from 'class-validator';

export class ChangeRolesDTO {
    @ApiModelProperty({example: ['Editor', 'Contributor']})
    @IsOptional()
    @IsArray({message: 'Roles should be array!'})
    public roles: string[];
}
