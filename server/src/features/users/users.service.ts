import { BadRequestException, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { plainToClass } from 'class-transformer';
import * as passwordHash from 'password-hash';
import { In, Repository } from 'typeorm';
import { RoleE } from '../../database/entities/role.entity';
import { UserE } from '../../database/entities/user.entity';
import { LoginDTO } from '../auth/models/login.dto';
import { RoleTypes } from '../roles/enums/role-types.enum';
import { ChangePasswordDTO } from './models/change-password.dto';
import { ChangeRolesDTO } from './models/change-roles.dto';
import { CreateUserDTO } from './models/create-user.dto';
import { ShowUserDTO } from './models/show-user.dto';
import { UpdateBanStatusDTO } from './models/update-ban-status.dto';
import { UpdateUserDTO } from './models/update-user.dto';
import { ResponseMessageDTO } from '../../common/models/response-message.dto';
import axios from 'axios';
import { LoggedUserPayload } from '../auth/models/logged-user-payload.dto';
import { SuppLangE } from '../../database/entities/supp-lang.entity';
import { UpdatePrefLangDTO } from './models/update-pref-lang.dto';
import { UpdateEditLanguagesDTO } from './models/update-edit-languages.dto';
import { ArticlesService } from '../articles/articles.service';
import { ArticleE } from '../../database/entities/article.entity';
import { ArticleVersionE } from '../../database/entities/article-version.entity';
import { ShowArticleVersionDTO } from '../articles/models/show-article.dto';
import { CommentE } from '../../database/entities/comment.entity';
import { ShowCommentDTO } from '../comments/models/show-comment.dto';

@Injectable()
export class UsersService {

    public constructor(
        @InjectRepository(UserE)
        private readonly usersRepository: Repository<UserE>,

        @InjectRepository(RoleE)
        private readonly rolesRepository: Repository<RoleE>,

        @InjectRepository(SuppLangE)
        private readonly suppLangRepository: Repository<SuppLangE>,

        @InjectRepository(CommentE)
        private readonly commentsRepository: Repository<CommentE>,

        private readonly articlesService: ArticlesService,

    ) {}

    public async getAllUsers(loggedUser: LoggedUserPayload): Promise<ShowUserDTO[]> {
        let users: UserE[];
        if (loggedUser.roles.includes('Admin')) {
            users = await this.usersRepository.find();
        } else {
            users = await this.usersRepository.find({
                where: {
                    isDeleted: false,
                },
            });
        }

        return plainToClass(ShowUserDTO, users, {
            excludeExtraneousValues: true,
        });
    }

    public async getAllArticleByUser(userId: string, lang: string): Promise<ArticleVersionE[]> {
        const articles: ArticleVersionE[] = await this.articlesService.getArticlesByUser(userId, lang);
        return articles;
    }

    public async createUser(newUser: CreateUserDTO): Promise<ShowUserDTO> {

        const { username, prefLang, password } = newUser;

        const role: RoleE = await this.rolesRepository.findOne({
            where: {
                name: RoleTypes.Contributor,
            },
        });

        const lang: SuppLangE = await this.suppLangRepository.findOne({
            where: {
                index: newUser.prefLang,
            },
        });

        const newUserEntity: UserE = this.usersRepository.create({username, password});
        newUserEntity.prefLang = lang;
        newUserEntity.roles = [role];
        newUserEntity.editLanguages = [];
        newUserEntity.profileImg = 'https://i.imgur.com/6036AIQ.png';
        newUserEntity.name = 'Name';
        newUserEntity.surname = 'Surname';
        newUserEntity.email = 'Email';
        newUserEntity.password = passwordHash.generate(newUserEntity.password);
        const savedUser = await this.usersRepository.save(newUserEntity);
        return plainToClass(ShowUserDTO, savedUser, {
                excludeExtraneousValues: true,
        });
    }

    public async updatedUser(loggedUser: LoggedUserPayload, userId: string, update: UpdateUserDTO) {
        if (!loggedUser.roles.includes('Admin') && loggedUser.id !== userId) {
            throw new BadRequestException('You are not authorized to do this action!');
        }
        let userToUpdate: UserE = await this.getUserEntityById(loggedUser, userId);

        userToUpdate = {...userToUpdate, ...update};

        await this.usersRepository.save(userToUpdate);

        return plainToClass(ShowUserDTO, userToUpdate, {
            excludeExtraneousValues: true,
        });
    }

    public async changeUserPassword(loggedUser: LoggedUserPayload, userId: string, changePasword: ChangePasswordDTO): Promise<ResponseMessageDTO> {
        if (!loggedUser.roles.includes('Admin') && loggedUser.id !== userId) {
            throw new BadRequestException('You are not authorized to do this action!');
        }
        const user = await this.getUserEntityById(loggedUser, userId);
        if (passwordHash.verify(changePasword.oldPass, user.password)) {
            user.password = passwordHash.generate(changePasword.newPass);
            await this.usersRepository.save(user);
            return {
                message: 'The password was changed succesffuly!',
                code: 200,
            };
        } else {
            return {
                message: `The old password doesn't match!`,
                code: 412,
            };
        }
    }

    public async changeRoles(loggedUser: LoggedUserPayload, userId: string, newRoles: ChangeRolesDTO) {
        const user = await this.getUserEntityById(loggedUser, userId);

        const rolesEntities: RoleE[] = await this.rolesRepository.find({
            name: In([...newRoles.roles]),
        });

        user.roles = rolesEntities;
        await this.usersRepository.save(user);
        return plainToClass(ShowUserDTO, user, {
            excludeExtraneousValues: true,
        });
    }

    public async updateEditLanguages(loggedUser: LoggedUserPayload, userId: string, editLanguages: UpdateEditLanguagesDTO) {
        const user: UserE = await this.getUserEntityById(loggedUser, userId);

        const langs: SuppLangE[] = await this.suppLangRepository.find({
            where: {
                name: In(editLanguages.editLanguages),
            },
        });

        user.editLanguages = langs;

        await this.usersRepository.save(user);

        return plainToClass(ShowUserDTO, user, {
            excludeExtraneousValues: true,
        });
    }

    public async banUser(loggedUser: LoggedUserPayload, userId: string, ban: UpdateBanStatusDTO): Promise<ShowUserDTO> {

        let userToBan: UserE = await this.getUserEntityById(loggedUser, userId);

        userToBan = {
            ...userToBan,
            isBanned: true,
            endOfBanDate: this.getBanDays(ban.days),
            banReason: ban.banReason,
        };

        await this.usersRepository.save(userToBan);

        return plainToClass(ShowUserDTO, userToBan, {
            excludeExtraneousValues: true,
        });
    }

    public async unBanUser(loggedUser: LoggedUserPayload, userId: string): Promise<ShowUserDTO> {
        let userToUnBan: UserE = await this.getUserEntityById(loggedUser, userId);

        userToUnBan = {
            ...userToUnBan,
            isBanned: false,
            endOfBanDate: null,
            banReason: '',
        };

        await this.usersRepository.save(userToUnBan);

        return plainToClass(ShowUserDTO, userToUnBan, {
            excludeExtraneousValues: true,
        });
    }

    public async deleteUser(loggedUser: LoggedUserPayload , userId: string): Promise<ShowUserDTO> {
        if (!loggedUser.roles.includes('Admin') && loggedUser.id !== userId) {
            throw new BadRequestException('You are not authorized to do this action!');
        }

        const userToDelete: UserE = await this.getUserEntityById(loggedUser, userId);

        userToDelete.isDeleted = !userToDelete.isDeleted;

        await this.usersRepository.save(userToDelete);

        return plainToClass(ShowUserDTO, userToDelete, {
            excludeExtraneousValues: true,
        });
    }

    public async getUserById(loggedUser: LoggedUserPayload, userId: string): Promise<ShowUserDTO> {
        return plainToClass(ShowUserDTO, await this.getUserEntityById(loggedUser, userId), {
            excludeExtraneousValues: true,
        });
    }

    public async authenticateLogin(userToLogin: LoginDTO): Promise<LoggedUserPayload> {
        const foundUser: UserE = await this.usersRepository.findOne({
            where: {
                username: userToLogin.username,
            },
        });

        if (!foundUser) {
            throw new BadRequestException('User with this username do not exist!');
        }

        if (foundUser.isDeleted) {
            throw new BadRequestException('No such user!');
        }

        if (foundUser.isBanned) {
            throw new BadRequestException('The User has been banned.');
        }

        if (await passwordHash.verify(userToLogin.password, foundUser.password)) {
            const { id, username, isBanned, banReason, endOfBanDate, powerPoints, profileImg } = foundUser;
            const roles = foundUser.roles.map((role: RoleE) => role.name);
            const prefLang = foundUser.prefLang.index;
            const editLanguages = foundUser.editLanguages.reduce((acc, curr) => {
                acc.push(curr.index);
                return acc;
            }, []);

            return { id, username, roles, isBanned, banReason, endOfBanDate, powerPoints, editLanguages , profileImg, prefLang };
        }
        throw new BadRequestException('Entered password is invalid!');
    }

    public async findByUsername(username: string): Promise<UserE> {

        const foundUser = await this.usersRepository.findOne({
            where: {
                username,
                isDeleted: false,
            },
        });

        if (!foundUser) {
            throw new BadRequestException('User not found');
        }

        return foundUser;
    }

    public async updatedUserPrefLang(loggedUser: LoggedUserPayload, userId: string, update: UpdatePrefLangDTO): Promise<ShowUserDTO> {
        if (!loggedUser.roles.includes('Admin') && loggedUser.id !== userId) {
            throw new BadRequestException('You are not authorized to do this action!');
        }

        const lang: SuppLangE = await this.suppLangRepository.findOne({
            where: {
                index: update.prefLang,
            },
        });

        const user: UserE = await this.usersRepository.findOne({
            where: {
                id: userId,
            },
        });

        user.prefLang = lang;
        await this.usersRepository.save(user);

        return plainToClass(ShowUserDTO, user, {
            excludeExtraneousValues: true,
        });
    }

    public async updatedUserImage(userId: string, updatedUser: UpdateUserDTO): Promise<ShowUserDTO> {
        const user: UserE = await this.usersRepository.findOne({
            where: {
                id: userId,
            },
        });
        const { data } = await axios(`https://api.imgur.com/3/upload`, {
            method: 'POST',
            headers: {
                'Authorization': 'Client-ID ad9cb2b2528002a',
                'Content-Type': 'multipart/form-data',
            },
            data: updatedUser.profileImg,
        });
        user.profileImg = data.data.link;
        await this.usersRepository.save(user);
        return plainToClass(ShowUserDTO, user, {
            excludeExtraneousValues: true,
        });
    }

    public async getUserComments(userId: string, lang: string): Promise<ShowCommentDTO[]> {
        const comments: CommentE[] = await this.commentsRepository.find({
            where: {
                user: userId,
            },
            relations: ['language'],
        });

        const filtredComments = comments.filter(async comment => {
            const language = await comment.language;
            if (language.index === lang) {
                return comment;
            }
        });

        return plainToClass(ShowCommentDTO, filtredComments, {
            excludeExtraneousValues: true,
        });
    }

    private async getUserEntityById(loggedUser: LoggedUserPayload, userId: string): Promise<UserE> {
        let user: UserE;
        if (loggedUser.roles.includes('Admin')) {
            user = await this.usersRepository.findOne({
                where: {
                    id: userId,
                },
            });
        } else {
            user = await this.usersRepository.findOne({
                where: {
                    id: userId,
                    isDeleted: false,
                },
            });
        }

        if (!user) {
            throw new BadRequestException('No such user!');
        }

        return user;
    }

    private getBanDays(days: number) {

        const now: number = new Date().getTime();

        return new Date(now + days * 24 * 60 * 60 * 1000);

    }
}
