
import { Injectable } from '@nestjs/common';
import { PassportStrategy } from '@nestjs/passport';
import { plainToClass } from 'class-transformer';
import { ExtractJwt, Strategy } from 'passport-jwt';
import { ConfigService } from '../../../config/config.service';
import { UserE } from '../../../database/entities/user.entity';
import { ShowUserDTO } from '../../users/models/show-user.dto';
import { UsersService } from '../../users/users.service';

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
  constructor(
    private readonly usersService: UsersService,
    configService: ConfigService,
  ) {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      secretOrKey: configService.jwtSecret,
      ignoreExpiration: false,
    });
  }

  public async validate(payload: ShowUserDTO): Promise<ShowUserDTO> {
    const user: UserE = await this.usersService.findByUsername(
      payload.username,
    );

    if (!user) {
      // throw new UnauthorizedException();
    }

    return plainToClass(ShowUserDTO, user, {
        excludeExtraneousValues: true,
    });
  }
}
