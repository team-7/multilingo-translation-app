import { IsNotEmpty, IsString } from 'class-validator';
import { ApiModelProperty } from '@nestjs/swagger';

export class LoginDTO {

    @ApiModelProperty({example: 'antogada'})
    @IsString()
    @IsNotEmpty()
    public username: string;

    @ApiModelProperty({example: '12345678'})
    @IsString()
    @IsNotEmpty()
    public password: string;
}
