import { Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { BlacklistService } from '../users/blacklist.service';
import { UsersService } from '../users/users.service';
import { LoginDTO } from './models/login.dto';
import { TokenDTO } from './models/token.dto';
import { LoggedUserPayload } from './models/logged-user-payload.dto';

@Injectable()
export class AuthService {
    constructor(
        private readonly jwtService: JwtService,
        private readonly userService: UsersService,
        private readonly blacklistService: BlacklistService,
      ) {}

    public async login(user: LoginDTO): Promise<TokenDTO> {
        const payload: LoggedUserPayload = await this.userService.authenticateLogin(user);
        return {
            token: await this.jwtService.signAsync(payload),
        };
    }

    public async logout(token: string): Promise<TokenDTO> {
        return await this.blacklistService.blacklistToken(token);
    }

    public async isTokenBlacklisted(token: TokenDTO): Promise<boolean> {
        return await this.blacklistService.isBlacklisted(token);
    }

    public async isAdmin(user: LoggedUserPayload) {
        return user.roles.includes('Admin');
    }

    public async verifyToken(token: string) {
        await this.jwtService.verifyAsync(token.split(' ')[1]);
    }

}
