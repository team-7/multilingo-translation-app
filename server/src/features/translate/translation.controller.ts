import { Body, Controller, Get, Param, Post, Put, Query, UseGuards, UseInterceptors } from '@nestjs/common';
import { User } from '../../common/decorators/user.decorator';
import { AuthGuardWithBlacklisting } from '../../common/guards/blacklist.guard';
import { NotLoggedGuard } from '../../common/guards/not-logged.guard';
import { TranslationE } from '../../database/entities/translation.entity';
import { TransformInterceptor } from '../../transformer/interceptors/transform.interceptor';
import { LoggedUserPayload } from '../auth/models/logged-user-payload.dto';
import { GoogleTranslateService } from './googleTranslate.service';
import { AllTranslationsDTO } from './models/all-translations.dto';
import { DetectionDTO } from './models/detection.dto';
import { ShowTranslationDTO } from './models/show-translation.dto';
import { TranslationService } from './translation.service';

@Controller('translations')
export class TranslationController {

    constructor(
        private readonly googleTranslateService: GoogleTranslateService,
        private readonly translationService: TranslationService,
    ) {}

    @Get()
    @UseInterceptors(new TransformInterceptor(ShowTranslationDTO))
    public async getAll(
        @Query('type') type: string,
    ): Promise<AllTranslationsDTO[]> {
        return this.translationService.getAll(type);

    }

    @Post('detection')
    public async detectLanguage(
        @Body() body: {text: string},
    ): Promise<DetectionDTO> {
        return await this.googleTranslateService.detectLanguage(body.text);
    }

    @Put(':id')
    @UseInterceptors(new TransformInterceptor(ShowTranslationDTO))
    @UseGuards(AuthGuardWithBlacklisting, NotLoggedGuard)
    public async updateTranslation(
        @Param('id') id: string,
        @Body() body: {text: string},
        @User() user: LoggedUserPayload,
    ): Promise<TranslationE> {
        return this.translationService.updateTranslation(id, body.text, user);
    }

    @Post('translate')
    @UseGuards(AuthGuardWithBlacklisting, NotLoggedGuard)
    public async translate(
        @Body() body: any) {
        return this.googleTranslateService.translate(body.text, {from: body.source, to: body.target});
    }

}
