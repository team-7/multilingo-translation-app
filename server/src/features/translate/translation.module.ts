import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ConfigService } from '../../config/config.service';
import { RatingE } from '../../database/entities/rating.entity';
import { SuppLangE } from '../../database/entities/supp-lang.entity';
import { TranslationE } from '../../database/entities/translation.entity';
import { UserE } from '../../database/entities/user.entity';
import { GoogleTranslateService } from './googleTranslate.service';
import { TranslationController } from './translation.controller';
import { TranslationService } from './translation.service';

@Module({
  imports: [
    TypeOrmModule.forFeature([TranslationE, SuppLangE, RatingE, UserE]),
  ],
  providers: [
    TranslationService,
    ConfigService,
    GoogleTranslateService,
  ],
  exports: [TranslationService, GoogleTranslateService],
  controllers: [TranslationController],
})
export class TranslationModule {}
