import { BadRequestException, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { RatingE } from '../../database/entities/rating.entity';
import { SuppLangE } from '../../database/entities/supp-lang.entity';
import { TranslationE } from '../../database/entities/translation.entity';
import { UserE } from '../../database/entities/user.entity';
import { LoggedUserPayload } from '../auth/models/logged-user-payload.dto';
import { GoogleTranslateService } from './googleTranslate.service';
import { AllTranslationsDTO } from './models/all-translations.dto';

@Injectable()
export class TranslationService {

    constructor(
        @InjectRepository(TranslationE) private readonly translRepo: Repository<TranslationE>,
        @InjectRepository(SuppLangE) private readonly langRepo: Repository<SuppLangE>,
        @InjectRepository(RatingE) private readonly ratingsRepo: Repository<RatingE>,
        @InjectRepository(UserE) private readonly usersRepo: Repository<UserE>,
        private readonly googleTranslateService: GoogleTranslateService,
    ) {
    }

    public async getAll(type: string): Promise<AllTranslationsDTO[]> {
        if (type !== 'article' && type !== 'ui-component') {
            throw new BadRequestException('Invalid translation type');
        }
        const foundTranslations: TranslationE[] = await this.translRepo
            .createQueryBuilder('translation')
            .leftJoinAndSelect('translation.originalLang', 'origLang')
            .leftJoinAndSelect('translation.targetLang', 'targetLang')
            .where('origLang.id != targetLang.id')
            .andWhere('type = :t', { t: type })
            .getMany();

        if (foundTranslations.length === 0) {
            return [];
        }

        const returnTranslations: AllTranslationsDTO[] = [];
        await Promise.all(foundTranslations.map(async (e) => {

            const avg: number = (await this.ratingsRepo
                .createQueryBuilder()
                .where('translationId = :id', {id: e.id})
                .select('AVG(rate)', 'avg')
                .getRawOne()
                ).avg;

            if (!(await e.translatedBy)) {
                e.translatedBy = {} as Promise<UserE>;
            }

            returnTranslations.push({...e, rating: avg ? avg : 0 });
        }));
        return returnTranslations;

    }

    public async getTranslationTo(text: string, lang: SuppLangE, typeComp: string): Promise<string> {
        const foundTransl: TranslationE = await this.translRepo.findOne({
            relations: ['targetLang'],
            where: {
                originalText: text,
                targetLang: lang,
                type: typeComp,
            },
        });

        return foundTransl ? foundTransl.translatedText : undefined;
    }

    public async translateByGoogleTranslate(text: string | string[], to: string) {
        const [translation] = await this.googleTranslateService.translate(text, to);

        return translation;
    }

    public async saveTranslationToDb(text: string, toLang: SuppLangE, translation: string, type: string): Promise<TranslationE> {
        const detected = await this.googleTranslateService.detectLanguage(text);
        const sourceLang: SuppLangE = await this.langRepo.findOne({
            where: {
                index: detected.language,
            },
        });
        const createdTransl: TranslationE = this.translRepo.create({
            originalText: text,
            translatedText: translation,
            type,
        });
        createdTransl.originalLang = Promise.resolve(sourceLang);
        createdTransl.targetLang = Promise.resolve(toLang);

        return await this.translRepo.save(createdTransl);
    }

    public async updateTranslation(translId: string, text: string, user: LoggedUserPayload): Promise<TranslationE> {
        const foundTransl: TranslationE = await this.translRepo.findOne({
            where: {
                id: translId,
            },
        });

        if (!foundTransl) {
            throw new BadRequestException('No translation found');
        }

        if (foundTransl.translatedText === text) {
            return foundTransl;
        }

        const foundRatingsIds: string[] = (await this.ratingsRepo.find({
            where: {
                translation: foundTransl,
            },
            relations: ['translation'],
        })).map((e: RatingE) => e.id);

        if (foundRatingsIds.length) {

            this.ratingsRepo.delete(foundRatingsIds);
        }

        const foundUser: UserE = await this.usersRepo.findOne({
            where: {
                id: user.id,
            },
        });

        foundTransl.translatedText = text;
        foundTransl.translatedBy = Promise.resolve(foundUser);

        const savedTranslation: TranslationE = await this.translRepo.save(foundTransl);

        return savedTranslation;
    }
}
