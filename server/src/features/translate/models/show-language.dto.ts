import { Publish } from '../../../transformer/decorators/publish';

export class ShowLanguageDTO {

    @Publish()
    id: string;

    @Publish()
    name: string;

    @Publish()
    index: string;

    @Publish()
    supported: boolean;
}
