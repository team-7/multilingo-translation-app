export class DetectionDTO {
    confidence: number;
    language: string;
    input: string;
}
