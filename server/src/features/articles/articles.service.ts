import { BadRequestException, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import axios from 'axios';
import { Repository } from 'typeorm';
import { ArticleCategoryE } from '../../database/entities/article-category.entity';
import { ArticleVersionE } from '../../database/entities/article-version.entity';
import { ArticleE } from '../../database/entities/article.entity';
import { SuppLangE } from '../../database/entities/supp-lang.entity';
import { TranslationE } from '../../database/entities/translation.entity';
import { UserE } from '../../database/entities/user.entity';
import { LoggedUserPayload } from '../auth/models/logged-user-payload.dto';
import { TranslationService } from '../translate/translation.service';
import { CreateArticleDTO } from './models/create-article.dto';
import { TranslationContentDTO } from './models/translation-content.dto';
import { UpdateArticleDTO } from './models/update-article.dto';

@Injectable()
export class ArticlesService {

    constructor(
        @InjectRepository(ArticleE) private readonly articleRepo: Repository<ArticleE>,
        @InjectRepository(SuppLangE) private readonly suppLangRepo: Repository<SuppLangE>,
        @InjectRepository(ArticleVersionE) private readonly articleVersionRepo: Repository<ArticleVersionE>,
        @InjectRepository(ArticleCategoryE) private readonly articleCategoryRepo: Repository<ArticleCategoryE>,
        @InjectRepository(UserE) private readonly usersRepo: Repository<UserE>,

        private readonly translationService: TranslationService,
    ) {}

    public async getArticlesByUser(userId: string, lang: string): Promise<ArticleVersionE[]> {

        const foundUser: UserE = await this.usersRepo.findOne({
            where: {
                id: userId,
            },
        });

        if (!foundUser) {
            throw new BadRequestException('No such user!');
        }

        const foundArticles: ArticleE[] = await this.articleRepo.find({
            where: {
                user: userId,
                isDeleted: false,
            },
            relations: ['user'],
        });

        if (!foundArticles.length) {
            return [];
        }

        const allUserArticles: ArticleVersionE[] = await Promise.all(foundArticles.map(async (e) => {
            return await this.getArticleCurrVersion(e.id, lang);
        }));
        return allUserArticles;
    }

    public async getAll(language: string): Promise<ArticleVersionE[]> {
        const foundArticles: ArticleE[] = await this.articleRepo.find({
            where: {
                isDeleted: false,
            },
        });

        if (!foundArticles.length) {
            return [];
        }

        const allArticlesLatestVersions: ArticleVersionE[] = await Promise.all(
            foundArticles.map(async (e) => {
                return await this.getArticleCurrVersion(e.id, language);
                },
            ),
        );

        return allArticlesLatestVersions;

    }

    public async getArticleCurrVersion(articleId: string, language?: string): Promise<ArticleVersionE> {
        const article = await this.articleRepo.findOne({
            where: {
                id: articleId,
                isDeleted: false,
            },
            relations: ['user', 'category'],
        });

        if (!article) {
            throw new BadRequestException('Not such article /its deleted/!');
        }

        const currVersion = await this.articleVersionRepo.findOne({
            where: {
                article: articleId,
                isCurrent: true,
            },
            relations: ['article', 'updatedBy'],
        });

        const articleLanguage = await article.language;
        const articleCategory = await article.category;
        console.log(articleLanguage);
        console.log(articleCategory);

        // If original language of the article is the same as requested return the original article
        if (language === articleLanguage.index || !language) {
            article.category = Promise.resolve(articleCategory);
            currVersion.article = Promise.resolve(article);

            return currVersion;
        }
        // Else

        const toLanguage: SuppLangE = await this.suppLangRepo.findOne({
            where: {
                index: language,
                supported: true,
            },
        });

        if (!toLanguage) {
            throw new BadRequestException('No such supported language');
        }

        const toTranslate: TranslationContentDTO = {
            title: currVersion.title,
            subtitle: currVersion.subtitle,
            content: currVersion.content,
            category: articleCategory.name,
            langName: articleLanguage.name,
        };
        const translatedContent: TranslationContentDTO = await this.translateContent(toTranslate, toLanguage);
        currVersion.title = translatedContent.title;
        currVersion.subtitle = translatedContent.subtitle;
        currVersion.content = translatedContent.content;

        articleCategory.name = translatedContent.category;
        articleLanguage.name = translatedContent.langName;
        article.category = Promise.resolve(articleCategory);
        article.language = Promise.resolve(articleLanguage);
        currVersion.article = Promise.resolve(article);

        return currVersion;
    }

    public async getLatestArticles(lang: string) {

        const foundArticles: ArticleE[] = await this.articleRepo.find({
            where: {
                isDeleted: false,
            },
            order: {
                createdOn: 'ASC',
            },
            take: 20,
        });

        if (!foundArticles.length) {
            return [];
        }

        const latestFoundArticles: ArticleVersionE[] = await Promise.all(foundArticles.map(async (e) => {
            return await this.getArticleCurrVersion( e.id, lang);
        }));

        return latestFoundArticles;
    }

    public async getAllFromCategory(articleCategory: string, language: string): Promise<ArticleVersionE[]> {

        const foundCategory: ArticleCategoryE = await this.articleCategoryRepo.findOne({
            where: {
                name: articleCategory,
            },
        });

        if (!foundCategory) {
            throw new BadRequestException('Category not found');
        }

        const articlesFromCategory: ArticleE[] = await this.articleRepo.find({
            where: {
                category: {
                    id: foundCategory.id,
                },
                isDeleted: false,
            },
            relations: ['category'],
        });

        if (!articlesFromCategory.length) {
            return [];
        }

        const allCurrentVersionsFromCategory: ArticleVersionE[] = await Promise.all(
            articlesFromCategory.map(async (e) => {
                return await this.getArticleCurrVersion(e.id, language);
            }),
        );

        return allCurrentVersionsFromCategory;
    }

    public async createArticle(article: CreateArticleDTO, user: LoggedUserPayload): Promise<ArticleVersionE> {
        const articleLang: SuppLangE = await this.suppLangRepo.findOne({
            where: {
                index: article.language,
                supported: true,
            },
        });

        if (!articleLang) {
            throw new BadRequestException('No such supported lang');
        }

        const foundCategory: ArticleCategoryE = await this.articleCategoryRepo.findOne({
            where: {
                id: article.category,
            },
        });

        const foundUser: UserE = await this.usersRepo.findOne({
            where: {
                id: user.id,
            },
        });

        if (!foundUser) {
            throw new BadRequestException('No such user');
        }

        // create article
        const createdArticle: ArticleE = this.articleRepo.create();
        createdArticle.user = Promise.resolve(foundUser);
        createdArticle.language = Promise.resolve(articleLang);
        createdArticle.category = Promise.resolve(foundCategory);
        const savedArticle: ArticleE = await this.articleRepo.save(createdArticle);

        const { data } = await axios(encodeURI(`https://api.imgur.com/3/upload`), {
            method: 'POST',
            headers: {
                'Authorization': 'Client-ID ad9cb2b2528002a',
                'Content-Type': 'multipart/form-data',
            },
            data: article.image,
            });

            // create first version of the article
        const firstVersion: ArticleVersionE = this.articleVersionRepo.create({
            title: article.title,
            subtitle: article.subtitle,
            content: article.content,
            versionNum: 1,
            updatedBy: null,
            image: data.data.link,
        });
        firstVersion.article = Promise.resolve(savedArticle);
        const firstVersionSaved: ArticleVersionE = await this.articleVersionRepo.save(firstVersion);

        const { language, image, category, ...translContent } = article;
        const langName = articleLang.name;

        await this.saveContentToAllLanguages({...translContent, langName});

        return firstVersionSaved;
    }

    private async saveContentToAllLanguages(article: TranslationContentDTO) {
        try {
        const supportedLanguages: SuppLangE[] = await this.suppLangRepo.find({ where: { supported: true } });
        await Promise.all(supportedLanguages.map(async (lang: SuppLangE) => {
                for (const key of Object.keys(article)) {
                    // const type: string = key === 'category' ? 'category' : 'article';
                    const type: string = 'article';
                    const foundTranslInDb = await this.translationService.getTranslationTo(article[key], lang, type);

                    if (!foundTranslInDb) {
                        const translatedText: string = await this.translationService.translateByGoogleTranslate(article[key], lang.index);
                        const savedTransl: TranslationE = await this.translationService.saveTranslationToDb(article[key], lang, translatedText, type);
                    }
                }
        }));
        } catch (error) {}
    }

    private async translateContent(articleContent: TranslationContentDTO, toLang: SuppLangE) {
        for await (const key of Object.keys(articleContent)) {
            articleContent[key] = await this.translationService.getTranslationTo(articleContent[key], toLang, 'article');
        }
        return {
            ...articleContent,
        };
    }

    public async updateArticle(articleId: string, body: UpdateArticleDTO, user: LoggedUserPayload): Promise<ArticleVersionE> {
        const articleToUpdate: ArticleE = await this.articleRepo.findOne({
            where: {
                id: articleId,
                isDeleted: false,
            },
            relations: ['user', 'category'],
        });

        if (!articleToUpdate) {
            throw new BadRequestException('No such article/deleted');
        }

        const articleAuthor: UserE = await articleToUpdate.user;

        if (articleAuthor.id !== user.id || !user.roles.includes('Admin')) {
            throw new BadRequestException('You cant update this article');
        }

        const { category, image, ...updatedContent } = body;

        const foundVersion = await this.articleVersionRepo.findOne({
            where: {
                article: articleId,
                ...updatedContent,
                image,
            },
            relations: ['article'],
        });

        // or maybe return msg that the article exists in a prev version
        if (foundVersion) {
            throw new BadRequestException('The updated article is same as article version number ' + foundVersion.versionNum);
        }

        if ((await articleToUpdate.category).name !== category) {
            const foundCategory: ArticleCategoryE[] = await this.articleCategoryRepo.find({
                where: {
                    name: category,
                },
            });

            if (!foundCategory) {
                throw new BadRequestException('No such category');
            }

            // articleToUpdate.category = Promise.resolve(foundCategory);
        }
        const foundUser: UserE = await this.usersRepo.findOne({
            where: {
                id: user.id,
            },
        });

        let versionNumber: number = await this.articleVersionRepo.count({ where: { article: articleId }});

        const createdVersion: ArticleVersionE = this.articleVersionRepo.create({
            versionNum: ++versionNumber,
            ...updatedContent,
        });
        articleToUpdate.language = Promise.resolve(await articleToUpdate.language);
        createdVersion.article = Promise.resolve(articleToUpdate);
        createdVersion.updatedBy = Promise.resolve(foundUser);
        const savedArticle: ArticleE = await this.articleRepo.save(articleToUpdate);

        const oldNewest: ArticleVersionE = await this.articleVersionRepo.findOne({
            where: {
                article: articleId,
                isCurrent: true,
            },
            relations: ['article'],
        });

        oldNewest.isCurrent = false;

        if (oldNewest.image !== image) {
            const { data } = await axios(encodeURI(`https://api.imgur.com/3/upload`), {
            method: 'POST',
            headers: {
                'Authorization': 'Client-ID ad9cb2b2528002a',
                'Content-Type': 'multipart/form-data',
            },
            data: body.image,
            });

            createdVersion.image = data.data.link;
        } else {
            createdVersion.image = image;
        }

        let savedNewVersion: ArticleVersionE;
        try {
            savedNewVersion = await this.articleVersionRepo.save(createdVersion);
            await this.articleVersionRepo.save(oldNewest);
            const langName = (await savedArticle.language).name;
            await this.saveContentToAllLanguages({...body, langName});
        } catch (error) {
            throw new BadRequestException(error.msg);
        }

        return savedNewVersion;
    }

    public async getArticleVersions(articleId: string, user: LoggedUserPayload): Promise<ArticleVersionE[]> {

        const foundVersions: ArticleVersionE[] = await this.articleVersionRepo.find({
            where: {
                article: articleId,
            },
            relations: ['article'],
        });

        const articleAuthorId: string = (await (await foundVersions[0].article).user).id;

        if (!user.roles.includes('Admin') && articleAuthorId !== user.id) {
            throw new BadRequestException('You are not allowed to get all versions');
        }

        return foundVersions;
    }

    public async revertVersion(articleId: string, articleVerId: string, user: LoggedUserPayload): Promise<ArticleVersionE> {

        const foundVersion: ArticleVersionE = await this.articleVersionRepo.findOne({
            where: {
                id: articleVerId,
            },
        });

        const articleAuthorId: string = (await (await foundVersion.article).user).id;

        if (!user.roles.includes('Admin') || articleAuthorId !== user.id) {
            throw new BadRequestException('You cant revert this article');
        }

        const currentVersion: ArticleVersionE = await this.articleVersionRepo.findOne({
            where: {
                isCurrent: true,
                article: articleId,
            },
            relations: ['article'],
        });

        try {
            currentVersion.isCurrent = false;
            foundVersion.isCurrent = true;
            this.articleVersionRepo.save([foundVersion, currentVersion]);
        } catch (error) {
            return currentVersion;
        }

        return foundVersion;

    }

    public async deleteArticle(articleId: string) {
        const foundArticle = await this.articleRepo.findOne({
            where: {
                id: articleId,
                isDeleted: false,
            },
        });

        if (!foundArticle) {
            throw new BadRequestException('Article do not exists/is deleted');
        }

        foundArticle.isDeleted = true;
        const deletedArticle: ArticleE = await this.articleRepo.save(foundArticle);

        return { msg: 'Article successfully deleted' };
    }

}
