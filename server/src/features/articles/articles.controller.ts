import { Body, Controller, Delete, Get, Param, Post, Put, Query, UseGuards, UseInterceptors, ValidationPipe } from '@nestjs/common';
import { ApiBearerAuth, ApiImplicitQuery, ApiUseTags } from '@nestjs/swagger';
import { User } from '../../common/decorators/user.decorator';
import { AuthGuardWithBlacklisting } from '../../common/guards/blacklist.guard';
import { NotLoggedGuard } from '../../common/guards/not-logged.guard';
import { ArticleVersionE } from '../../database/entities/article-version.entity';
import { TransformInterceptor } from '../../transformer/interceptors/transform.interceptor';
import { LoggedUserPayload } from '../auth/models/logged-user-payload.dto';
import { ArticlesService } from './articles.service';
import { CreateArticleDTO } from './models/create-article.dto';
import { ShowArticleVersionDTO } from './models/show-article.dto';
import { ShowVersionDTO } from './models/show-version.dto';
import { UpdateArticleDTO } from './models/update-article.dto';

@ApiUseTags('Article Controller')
@ApiBearerAuth()
@Controller('articles')
export class ArticlesController {

    constructor(
        private readonly articlesService: ArticlesService,
    ) {}

    @Get('latest')
    @UseInterceptors(new TransformInterceptor(ShowArticleVersionDTO))
    public async getLatestArticles(
        @Query('lang') lang: string,
    ) {
        return await this.articlesService.getLatestArticles(lang);
    }

    @ApiImplicitQuery({
        name: 'category',
        required: false,
    })
    @Get()
    @UseInterceptors(new TransformInterceptor(ShowArticleVersionDTO))
    public async getArticles(
        @Query('lang') language: string,
        @Query('category') category?: string,
    ): Promise<ArticleVersionE[]> {

        if (!category) {
            return await this.articlesService.getAll(language);
        }
        return await this.articlesService.getAllFromCategory(category, language);
    }

    @Get(':id')
    @UseInterceptors(new TransformInterceptor(ShowArticleVersionDTO))
    public async getSingleArticle(
       @Param('id') id: string,
       @Query('lang') language: string,
    ): Promise<ArticleVersionE> {
        return await this.articlesService.getArticleCurrVersion(id, language);
    }

    @Get(':id/versions')
    @UseGuards(AuthGuardWithBlacklisting, NotLoggedGuard)
    @UseInterceptors(new TransformInterceptor(ShowVersionDTO))
    public async getArticleVersions(
       @Param('id') id: string,
       @User() user: LoggedUserPayload,
    ): Promise<ArticleVersionE[]> {
        return await this.articlesService.getArticleVersions(id, user);
    }

    @Put(':id/versions')
    @UseGuards(AuthGuardWithBlacklisting, NotLoggedGuard)
    @UseInterceptors(new TransformInterceptor(ShowVersionDTO))
    public async revertVersion(
        @Param('id') articleId: string,
        @Body() version: { versionId: string },
        @User() user: LoggedUserPayload,
    ): Promise<ArticleVersionE> {
        return this.articlesService.revertVersion(articleId, version.versionId, user);
    }

    @Post()
    @UseGuards(AuthGuardWithBlacklisting, NotLoggedGuard)
    @UseInterceptors(new TransformInterceptor(ShowArticleVersionDTO))
    public async createArticle(
        @Body(new ValidationPipe({
            whitelist: true,
            transform: true,
          })) body: CreateArticleDTO,
        @User() user: LoggedUserPayload,
    ): Promise<ArticleVersionE> {
        return await this.articlesService.createArticle(body, user);
    }

    @Put(':id')
    @UseGuards(AuthGuardWithBlacklisting, NotLoggedGuard)
    @UseInterceptors(new TransformInterceptor(ShowArticleVersionDTO))
    public async updateArticle(
        @Param('id') id: string,
        @Body(new ValidationPipe({
            whitelist: true,
            transform: true,
          })) body: UpdateArticleDTO,
        @User() user: LoggedUserPayload,
    ): Promise<ArticleVersionE> {
        return this.articlesService.updateArticle(id, body, user);
    }

    @Delete(':id')
    public async deleteArticle(
        @Param('id') id: string,
    ): Promise<{msg: string}> {
        return this.articlesService.deleteArticle(id);
    }
}
