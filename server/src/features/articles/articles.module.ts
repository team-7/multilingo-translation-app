import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ArticleCategoryE } from '../../database/entities/article-category.entity';
import { ArticleVersionE } from '../../database/entities/article-version.entity';
import { ArticleE } from '../../database/entities/article.entity';
import { RatingE } from '../../database/entities/rating.entity';
import { SuppLangE } from '../../database/entities/supp-lang.entity';
import { TranslationE } from '../../database/entities/translation.entity';
import { UserE } from '../../database/entities/user.entity';
import { GoogleTranslateService } from '../translate/googleTranslate.service';
import { TranslationService } from '../translate/translation.service';
import { ArticlesController } from './articles.controller';
import { ArticlesService } from './articles.service';

@Module({
  imports: [
    TypeOrmModule.forFeature(
      [
        ArticleE,
        SuppLangE,
        ArticleVersionE,
        TranslationE,
        RatingE,
        ArticleCategoryE,
        UserE,
      ],
    ),
  ],
  controllers: [
    ArticlesController,
  ],
  providers: [
    ArticlesService,
    TranslationService,
    GoogleTranslateService,
  ],
  exports: [
    ArticlesService,
  ],
})
export class ArticlesModule {}
