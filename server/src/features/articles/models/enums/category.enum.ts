export enum ArticleCategory {
    Sport = 'sport',
    Politics = 'politics',
    EverydayNews = 'everydayNews',
}
