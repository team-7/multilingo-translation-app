import { Publish } from '../../../transformer/decorators/publish';

export class PublishUserDTO {
    @Publish()
    public id: string;

    @Publish()
    public username: string;

    @Publish()
    public name: string;

    @Publish()
    public surname: string;

    @Publish()
    public powerPoints: number;

    @Publish()
    public profileImg: string;

    @Publish()
    public isBanned: boolean;

}
