import { ArticleCategoryE } from '../../../database/entities/article-category.entity';
import { SuppLangE } from '../../../database/entities/supp-lang.entity';
import { UserE } from '../../../database/entities/user.entity';
import { Publish } from '../../../transformer/decorators/publish';
import { ShowCategoryDTO } from '../../category/models/show-category.dto';
import { PublishUserDTO } from './publish-user.dto';
import { ShowArticleLanguageDTO } from './show-article-language.dto';

export class ShowArticleInfoDTO {

    @Publish()
    id: string;

    @Publish()
    createdOn: string;

    @Publish(ShowCategoryDTO)
    category: ArticleCategoryE;

    @Publish(PublishUserDTO)
    user: UserE;

    @Publish(ShowArticleLanguageDTO)
    language: SuppLangE;
}
