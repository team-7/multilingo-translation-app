import { ArticleE } from '../../../database/entities/article.entity';
import { UserE } from '../../../database/entities/user.entity';
import { Publish } from '../../../transformer/decorators/publish';
import { PublishUserDTO } from './publish-user.dto';
import { ShowArticleInfoDTO } from './show-articleInfo.dto';

export class ShowArticleVersionDTO {
    @Publish()
    id: string;

    @Publish()
    versionNum: number;

    @Publish()
    title: string;

    @Publish()
    subtitle: string;

    @Publish()
    content: string;

    @Publish()
    updatedOn: string;

    @Publish()
    isCurrent: boolean;

    @Publish(PublishUserDTO)
    updatedBy: UserE | null;

    @Publish(ShowArticleInfoDTO)
    article: ArticleE;

    @Publish()
    image: string;

}
