import { ApiModelProperty } from '@nestjs/swagger';
import { IsString, Length } from 'class-validator';

export class UpdateArticleDTO {

    @ApiModelProperty({example: 'New update title'})
    @IsString()
    @Length(5, 100)
    title: string;

    @ApiModelProperty({example: 'New update subtitle'})
    @IsString()
    @Length(5, 100)
    subtitle: string;

    @ApiModelProperty({example: 'New update content'})
    @IsString()
    @Length(20, 3000)
    content: string;

    @ApiModelProperty({example: 'sport'})
    @IsString()
    category: string;

    @IsString()
    image: string;
}
