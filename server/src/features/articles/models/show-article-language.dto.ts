import { Publish } from '../../../transformer/decorators/publish';

export class ShowArticleLanguageDTO {

    @Publish()
    name: string;

    @Publish()
    index: string;
}
