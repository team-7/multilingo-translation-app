export class CreateRatingDTO {
    text: string;
    originalLang: string;
    toLang: string;
    rate: number;
}
