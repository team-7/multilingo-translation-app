export class PhraseRatingDTO {
    avg: number;
    sum: number;
    count: number;
    myRate: number;
}
