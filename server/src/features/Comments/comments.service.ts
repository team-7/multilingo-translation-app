import { BadRequestException, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { plainToClass } from 'class-transformer';
import { Repository } from 'typeorm';
import { ArticleVersionE } from '../../database/entities/article-version.entity';
import { ArticleE } from '../../database/entities/article.entity';
import { CommentE } from '../../database/entities/comment.entity';
import { SuppLangE } from '../../database/entities/supp-lang.entity';
import { TranslationE } from '../../database/entities/translation.entity';
import { UserE } from '../../database/entities/user.entity';
import { LoggedUserPayload } from '../auth/models/logged-user-payload.dto';
import { GoogleTranslateService } from '../translate/googleTranslate.service';
import { CreateCommentDTO } from './models/create-comment.dto';
import { ShowCommentDTO } from './models/show-comment.dto';
import { UpdateCommentDTO } from './models/update-comment.dto';

@Injectable()
export class CommentsService {

    public constructor(
        @InjectRepository(CommentE) private readonly commentRepo: Repository<CommentE>,
        @InjectRepository(UserE) private readonly userRepo: Repository<UserE>,
        @InjectRepository(ArticleE) private readonly articleRepo: Repository<ArticleE>,
        @InjectRepository(ArticleVersionE) private readonly articleVersionRepo: Repository<ArticleVersionE>,
        @InjectRepository(SuppLangE) private readonly suppLangRepo: Repository<SuppLangE>,
        @InjectRepository(TranslationE) private readonly translationsRepo: Repository<TranslationE>,
        private readonly googleTranslateClient: GoogleTranslateService,
    ) {}

    public async getAllCommentsByArticle(articleId: string, language: string): Promise<ShowCommentDTO[]> {

        const foundArticle = await this.articleRepo.findOne({
            where: {
                id: articleId,
                isDeleted: false,
            },
        });
        if (!foundArticle) {
            throw new BadRequestException('Not such article /its deleted/!');
        }

        const comments: CommentE[] = await this.commentRepo.find({
            where: {
                article: foundArticle,
                isDeleted: false,
                order: {
                    createdOn: 'ASC',
                },
            },
        });

        const articleLanguage = await foundArticle.language;
        // If original language of the article is the same as requested return the original comment of the article
        // if (language === articleLanguage.index) {
        //     return plainToClass(ShowCommentDTO, comments, {
        //         excludeExtraneousValues: true,
        //       });
        // }
        // Else
        const reqLanguage = await this.suppLangRepo.findOne({ where: { index: language }});
        // Check

        comments.map(async o => {
            const commentLang = await o.language;

            if (commentLang.index === language) {
                return o;
            }
            const foundTranslation = await this.translationsRepo.findOne({
                where: {
                    originalText: o.content,
                    originalLang: commentLang,
                    targetLang: reqLanguage,
                    type: 'comment',
                },
            });

            o.content = foundTranslation.translatedText;
            return o;
        });
        return plainToClass(ShowCommentDTO, comments, {
            excludeExtraneousValues: true,
          });
    }

    public async createComment(body: CreateCommentDTO, user: LoggedUserPayload, articleId: string): Promise<ShowCommentDTO> {

        const articleEntity: ArticleE = await this.articleRepo.findOne({
            where: {
                id: articleId,
            },
        });

        const {language, ...comment} = body;
        const commentEntity: CommentE = this.commentRepo.create(comment);

        const userEntity: UserE = await this.userRepo.findOne({
            where: {
                username: user.username,
            },
        });
        const langs = await this.suppLangRepo.findOne({
            where: {
                index: body.language,
                supported: true,
            },
        });

        commentEntity.language = Promise.resolve(langs);
        commentEntity.user = Promise.resolve(userEntity);
        commentEntity.article = Promise.resolve(articleEntity);

        const suppLangs = await this.suppLangRepo.find({where: {
            supported: true,
        }});

        suppLangs.map(async o => {
            const foundTranslation = await this.translationsRepo.findOne({
                where: {
                    originalText: commentEntity.content,
                    originalLang: langs,
                    targetLang: o,
                    type: 'comment',
                },
            });

            if (!foundTranslation) {

                const [translation] = await this.googleTranslateClient.translate(commentEntity.content, o.index);

                const createdTransl = this.translationsRepo.create({
                originalText: commentEntity.content,
                translatedText: translation,
                type: 'comment',
            });

                createdTransl.originalLang = Promise.resolve(langs);
                createdTransl.targetLang = Promise.resolve(o);

                const savedTransl = await this.translationsRepo.save(createdTransl);

            }
                });
        const saved = await this.commentRepo.save(commentEntity);

        return plainToClass(ShowCommentDTO, saved, {
            excludeExtraneousValues: true,
          });
    }

    public async editComment(commentId: string, content: UpdateCommentDTO): Promise<ShowCommentDTO> {

        const commentEntity: CommentE = await this.commentRepo.findOne(commentId);

        const updatedComment: CommentE = { ...commentEntity, ...content };

        return plainToClass(ShowCommentDTO, await this.commentRepo.save(updatedComment), {
            excludeExtraneousValues: true,
        });
    }

    public async commentsByUser(userId: string): Promise<ShowCommentDTO[]> {

        const commentEntity: CommentE[] = await this.commentRepo.find({
            where: {
                user: userId,
            },
        });

        return plainToClass(ShowCommentDTO, commentEntity, {
            excludeExtraneousValues: true,
        });

    }
}
