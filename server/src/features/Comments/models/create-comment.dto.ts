import { ApiModelProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsString, Length } from 'class-validator';

export class CreateCommentDTO {

    @ApiModelProperty({ example: 'Mnogo qk text'})
    @IsString()
    @IsNotEmpty()
    @Length(0, 500)
    public content: string;

    @ApiModelProperty({ example: 'en'})
    @IsString()
    @IsNotEmpty()
    public language: string;
}
