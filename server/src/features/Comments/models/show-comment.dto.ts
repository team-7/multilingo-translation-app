import { Expose, Transform } from 'class-transformer';

export class ShowCommentDTO {

    @Expose()
    public id: string;

    @Expose()
    public content: string;

    @Expose()
    public createdOn: Date;

    @Expose()
    @Transform((_, obj) => obj.user.username)
    public user: string;
    
}
