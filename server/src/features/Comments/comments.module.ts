import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ArticleVersionE } from '../../database/entities/article-version.entity';
import { ArticleE } from '../../database/entities/article.entity';
import { CommentE } from '../../database/entities/comment.entity';
import { SuppLangE } from '../../database/entities/supp-lang.entity';
import { TranslationE } from '../../database/entities/translation.entity';
import { UserE } from '../../database/entities/user.entity';
import { GoogleTranslateService } from '../translate/googleTranslate.service';
import { CommentsController } from './comments.controller';
import { CommentsService } from './comments.service';

@Module({
    imports: [TypeOrmModule.forFeature([CommentE, UserE, ArticleE, ArticleVersionE, SuppLangE, TranslationE])],
    controllers: [CommentsController],
    providers: [
        CommentsService,
        GoogleTranslateService,
    ],
})
export class CommentsModule {}
