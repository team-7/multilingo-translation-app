import { Controller, Get, Query } from '@nestjs/common';
import { UiComponentsService } from './ui-components.service';

@Controller('ui-components')
export class UiComponentsController {
    constructor(
        private readonly uiService: UiComponentsService,
    ) {}

    @Get()
    public async getAllUIInLang(
        @Query('lang') language: string,
    ) {
        return this.uiService.getAllUIInLang(language);
    }

}
