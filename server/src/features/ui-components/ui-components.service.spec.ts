import { Test, TestingModule } from '@nestjs/testing';
import { UiComponentsService } from './ui-components.service';

describe('UiComponentsService', () => {
  let service: UiComponentsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [UiComponentsService],
    }).compile();

    service = module.get<UiComponentsService>(UiComponentsService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
