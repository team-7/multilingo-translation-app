import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { SuppLangE } from '../../database/entities/supp-lang.entity';
import { TranslationE } from '../../database/entities/translation.entity';
import { UIComponentE } from '../../database/entities/ui-element.entity';
import { UiComponentsController } from './ui-components.controller';
import { UiComponentsService } from './ui-components.service';

@Module({
  imports: [
    TypeOrmModule.forFeature([UIComponentE, SuppLangE, TranslationE]),
  ],
  controllers: [UiComponentsController],
  providers: [UiComponentsService],
})
export class UiComponentsModule {}
