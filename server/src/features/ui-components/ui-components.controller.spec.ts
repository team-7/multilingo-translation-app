import { Test, TestingModule } from '@nestjs/testing';
import { UiComponentsController } from './ui-components.controller';

describe('UiComponents Controller', () => {
  let controller: UiComponentsController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [UiComponentsController],
    }).compile();

    controller = module.get<UiComponentsController>(UiComponentsController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
