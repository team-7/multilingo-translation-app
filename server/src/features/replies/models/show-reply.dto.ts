import { Expose } from 'class-transformer';

export class ShowReplyDTO {

    @Expose()
    public id: string;

    @Expose()
    public content: string;

    @Expose()
    public createdOn: Date;

}
