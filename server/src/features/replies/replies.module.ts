import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { RepliesController } from './replies.controller';
import { RepliesService } from './replies.service';
import { ReplyE } from '../../database/entities/reply.entity';
import { CommentE } from '../../database/entities/comment.entity';

@Module({
    imports: [TypeOrmModule.forFeature([ReplyE, CommentE])],
    controllers: [RepliesController],
    providers: [RepliesService],
})
export class RepliesModule {}
