import { BadRequestException, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { plainToClass } from 'class-transformer';
import { Repository } from 'typeorm';
import { ResponseMessageDTO } from '../../common/models/response-message.dto';
import { CommentE } from '../../database/entities/comment.entity';
import { ReplyE } from '../../database/entities/reply.entity';
import { CreateCommentDTO } from '../comments/models/create-comment.dto';
import { ShowReplyDTO } from './models/show-reply.dto';
import { UpdateReplyDTO } from './models/update-reply.dto';
import { ArticleE } from '../../database/entities/article.entity';

@Injectable()
export class RepliesService {

    public constructor(
        @InjectRepository(ReplyE) private readonly replyRepo: Repository<ReplyE>,
        @InjectRepository(CommentE) private readonly commentRepo: Repository<CommentE>,
    ) {}

    public async getAllRepliesByCommentId(commentId: string): Promise<ShowReplyDTO[]> {
        const comment: CommentE = await this.commentRepo.findOne({
            where: {
                id: commentId,
            },
            relations: ['replies'],
        });

        const replies: ReplyE[] = await comment.replies;

        return plainToClass(ShowReplyDTO, replies, {
            excludeExtraneousValues: true,
          });
    }

    public async createReply(commentId: string, body: CreateCommentDTO): Promise<ShowReplyDTO> {

        const commentEntity: CommentE = await this.commentRepo.findOne(commentId);
        if (!commentEntity) {
            throw new BadRequestException('Comment does not exist!');
        }

        const reply: ReplyE = this.replyRepo.create(body);

        reply.comment = Promise.resolve(commentEntity);

        const saveEntity: ReplyE = await this.replyRepo.save(reply);

        return plainToClass(ShowReplyDTO, saveEntity, {
            excludeExtraneousValues: true,
          });
    }

    public async removeReply(replyId: string, commentId: string): Promise<ResponseMessageDTO> {

        const comment: CommentE = await this.commentRepo.findOne(commentId);
        if (!comment) {
            throw new BadRequestException('Comment does not exist!');
        }

        const replyEntity: ReplyE = await this.replyRepo.findOne(replyId);

        replyEntity.isDeleted = true;

        await this.replyRepo.save(replyEntity);

        return { message: 'Reply Removed!' };

    }

    public async editReply(replyId: string, commentId: string, content: UpdateReplyDTO): Promise<ShowReplyDTO> {

        const comment: CommentE = await this.commentRepo.findOne(commentId);
        if (!comment) {
            throw new BadRequestException('Comment does not exist!');
        }

        const replyEntity: ReplyE = await this.replyRepo.findOne(replyId);

        if (!replyEntity || replyEntity.isDeleted) {
            throw new BadRequestException('Reply does not exist!');
        }

        const updatedReply: ReplyE = { ...replyEntity, ...content };

        const saveEntity: ReplyE = await this.replyRepo.save(updatedReply);

        return plainToClass(ShowReplyDTO, saveEntity, {
            excludeExtraneousValues: true,
        });
    }
}
