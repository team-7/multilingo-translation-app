import { Expose } from 'class-transformer';

export class ShowRoleDTO {

    @Expose()
    public name: string;
}
