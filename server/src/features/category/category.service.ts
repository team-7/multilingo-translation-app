import { BadRequestException, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { ArticleCategoryE } from '../../database/entities/article-category.entity';
import { SuppLangE } from '../../database/entities/supp-lang.entity';
import { TranslationE } from '../../database/entities/translation.entity';
import { TranslationService } from '../translate/translation.service';

@Injectable()
export class CategoryService {
    constructor(
        @InjectRepository(ArticleCategoryE) private readonly categoryRepo: Repository<ArticleCategoryE>,
        @InjectRepository(SuppLangE) private readonly langRepo: Repository<SuppLangE>,
        @InjectRepository(TranslationE) private readonly translRepo: Repository<TranslationE>,
        private readonly translationService: TranslationService,
    ) {}

    public async getAllCategoriesInlang(language: string): Promise<ArticleCategoryE[]> {
        console.log(language);

        const foundLang: SuppLangE = await this.langRepo.findOne({
            where: {
                index: language,
                supported: true,
            },
        });

        if (!foundLang) {
            throw new BadRequestException('We do not support this lang yet');
        }

        const foundCategories: ArticleCategoryE[] = await this.categoryRepo.find();

        const allFoundCategoriesInLang: ArticleCategoryE[] = await Promise.all((foundCategories).map(async (e) => {

                const foundTransl: TranslationE = await this.translRepo.findOne({
                    where: {
                        targetLang: foundLang.id,
                        originalText: e.name,
                        type: 'article',
                    },
                    relations: ['targetLang', 'originalLang'],
                });

                e.name = foundTransl.translatedText;
                return e;
            }),
        );

        return allFoundCategoriesInLang;

     }

     public async getCategoryById(id: string): Promise<ArticleCategoryE> {
        const foundCategory: ArticleCategoryE = await this.categoryRepo.findOne({
            where: {
                id,
            },
        });

        if (!foundCategory) {
            throw new BadRequestException('Category does not exist');
        }

        return foundCategory;
     }

     public async addCategories(category: {name: string, lang: string}): Promise<ArticleCategoryE> {
        const name = category.name.toLowerCase();
        const foundCategory: ArticleCategoryE = await this.categoryRepo.findOne({
            where: {
                name,
            },
        });

        if (foundCategory) {
            throw new BadRequestException('Category already exists');
        }

        try {
            await this.saveContentToAllLanguages(category.name);
            const savedCategory: ArticleCategoryE = await this.categoryRepo.save({name: category.name});
            return savedCategory;
        } catch (error) {
            throw new BadRequestException('Cannot save category');
        }

     }

     private async saveContentToAllLanguages(categoryName: string) {
        const supportedLanguages: SuppLangE[] = await this.langRepo.find({ where: { supported: true } });
        await Promise.all(supportedLanguages.map(async (lang: SuppLangE) => {
            try {
                const foundTranslInDb = await this.translationService.getTranslationTo(categoryName, lang, 'article');
                if (!foundTranslInDb) {
                    const translatedText: string = await this.translationService.translateByGoogleTranslate(categoryName, lang.index);
                    const savedTransl: TranslationE = await this.translationService.saveTranslationToDb(categoryName, lang, translatedText, 'article');
                    return savedTransl;
                }
            } catch (error) { /* empty */ }

        }));
    }
}
