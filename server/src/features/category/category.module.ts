import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ArticleCategoryE } from '../../database/entities/article-category.entity';
import { RatingE } from '../../database/entities/rating.entity';
import { SuppLangE } from '../../database/entities/supp-lang.entity';
import { TranslationE } from '../../database/entities/translation.entity';
import { UserE } from '../../database/entities/user.entity';
import { GoogleTranslateService } from '../translate/googleTranslate.service';
import { TranslationService } from '../translate/translation.service';
import { CategoryController } from './category.controller';
import { CategoryService } from './category.service';

@Module({
  imports: [
    TypeOrmModule.forFeature([SuppLangE, TranslationE, ArticleCategoryE, RatingE, UserE]),
  ],
  controllers: [CategoryController],
  providers: [CategoryService, TranslationService, GoogleTranslateService],
})
export class CategoryModule {}
