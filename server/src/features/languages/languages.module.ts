import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ArticleVersionE } from '../../database/entities/article-version.entity';
import { ArticleE } from '../../database/entities/article.entity';
import { RatingE } from '../../database/entities/rating.entity';
import { SuppLangE } from '../../database/entities/supp-lang.entity';
import { TranslationE } from '../../database/entities/translation.entity';
import { GoogleTranslateService } from '../translate/googleTranslate.service';
import { LanguagesController } from './languages.controller';
import { LanguagesService } from './languages.service';

@Module({
  imports: [TypeOrmModule.forFeature([SuppLangE, TranslationE, ArticleVersionE, ArticleE, RatingE])],
  controllers: [LanguagesController],
  providers: [LanguagesService, GoogleTranslateService],
})
export class LanguagesModule {}
