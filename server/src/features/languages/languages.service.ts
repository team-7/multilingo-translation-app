import { BadRequestException, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { In, Repository } from 'typeorm';
import { ArticleVersionE } from '../../database/entities/article-version.entity';
import { ArticleE } from '../../database/entities/article.entity';
import { RatingE } from '../../database/entities/rating.entity';
import { SuppLangE } from '../../database/entities/supp-lang.entity';
import { TranslationE } from '../../database/entities/translation.entity';
import { GoogleTranslateService } from '../translate/googleTranslate.service';

@Injectable()
export class LanguagesService {
    constructor(
        @InjectRepository(SuppLangE) private readonly langRepo: Repository<SuppLangE>,
        @InjectRepository(TranslationE) private readonly translRepo: Repository<TranslationE>,
        @InjectRepository(ArticleE) private readonly articlesRepo: Repository<ArticleE>,
        @InjectRepository(ArticleVersionE) private readonly articleVerRepo: Repository<ArticleVersionE>,
        @InjectRepository(RatingE) private readonly ratingRepo: Repository<RatingE>,

        private readonly googleService: GoogleTranslateService,
    ) {}

    public async getAllLanguages(): Promise<SuppLangE[]> {
        const foundLanguages: SuppLangE[] = await this.langRepo.find();

        if (!foundLanguages) {
            throw new BadRequestException('No languages!');
        }
        return foundLanguages;
    }

    public async getSupportedLanguages(): Promise<SuppLangE[]> {
        const foundLanguages: SuppLangE[] = await this.langRepo.find({
            where: {
                supported: true,
            },
        });

        if (!foundLanguages) {
            throw new BadRequestException('No languages!');
        }
        return foundLanguages;
    }

    public async findAndChangeSupportStatus(names: string[]): Promise<SuppLangE[]> {
        const foundLanguages: SuppLangE[] = await this.langRepo.find({
            where: {
                name: In(names),
            },
        });

        await Promise.all(foundLanguages.map(async (language: SuppLangE) => {
            if (language.supported) {
                await this.deleteAllTranslationsForLanguage(language);
                await this.deleteAllArticlesAndVersionsInLanguage(language);

                language.supported = false;
                await this.langRepo.save(language);
                return language;
            } else {
                await this.addTranslationsForNewLanguage(language);
                language.supported = true;
                await this.langRepo.save(language);
                return language;
            }
        }));

        const savedLanguages: SuppLangE[] = await this.langRepo.save(foundLanguages);
        return savedLanguages;
    }

    private async addTranslationsForNewLanguage(language: SuppLangE) {
        const defaultLanguage: SuppLangE = await this.langRepo.findOne({
            where: {
                index: 'en',
            },
        });
        const allTranslations: TranslationE[] = await this.translRepo.find({
            where: {
                targetLangId: defaultLanguage.id,
            },
            relations: ['originalLang', 'targetLang'],
        });

        await Promise.all(allTranslations.map(async (transl: TranslationE) => {
            const currLanguage: SuppLangE = await transl.originalLang;
            const [translation] = await this.googleService.translate(
                transl.originalText,
                {
                    to: language.index,
                    from: currLanguage.index,
                },
            );
            const newTranslation: TranslationE = this.translRepo.create({
                originalText: transl.originalText,
                translatedText: translation,
                type: transl.type,
            });

            newTranslation.originalLang = Promise.resolve(currLanguage);
            newTranslation.targetLang = Promise.resolve(language);
            try {
                await this.translRepo.save(newTranslation);
            } catch (error) {
                throw new BadRequestException('Cant save translation');
            }

        }));
    }

    private async deleteAllTranslationsForLanguage(language: SuppLangE) {
        const foundTransl: TranslationE[] = await this.translRepo.find({
            where: [
                {
                    originalLang: language.id,
                },
                {
                    targetLang: language.id,
                },
            ],
            relations: ['originalLang', 'targetLang'],
        });
        const translIds: string[] = await Promise.all([...foundTransl].map(e => e.id));
        const foundRatings = await this.ratingRepo.find({
            where: {
                translationId: In(translIds),
            },
            relations: ['translation'],
        });
        if (foundRatings.length !== 0) {
            await this.ratingRepo.remove(foundRatings);
        }

        if (foundTransl.length !== 0) {

            await this.translRepo.remove(foundTransl);
        }

    }

    private async deleteAllArticlesAndVersionsInLanguage(language: SuppLangE) {
        const foundArticles: ArticleE[] = await this.articlesRepo.find({
            where: {
                language: language.id,
            },
            relations: ['language'],
        });

        const articlesIds: string[] = await Promise.all([...foundArticles].map(e => e.id));
        const foundVersions: ArticleVersionE[] = await this.articleVerRepo.find({
            where: {
                article: In(articlesIds),
            },
            relations: ['article'],
        });

        if (foundVersions.length !== 0) {
            await this.articleVerRepo.remove(foundVersions);
        }

        if (foundArticles.length !== 0) {
            await this.articlesRepo.remove(foundArticles);
        }
    }
}
