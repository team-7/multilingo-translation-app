import * as passwordHash from 'password-hash';
import { Connection, Repository } from 'typeorm';
import { RoleTypes } from '../../../features/roles/enums/role-types.enum';
import { RoleE } from '../../entities/role.entity';
import { SuppLangE } from '../../entities/supp-lang.entity';
import { UserE } from '../../entities/user.entity';

export const seedUsers = async (connection: Connection, defLanguage: SuppLangE) => {

    const roleRepo: Repository<RoleE> = connection.getRepository(RoleE);
    const userRepo: Repository<UserE> = connection.getRepository(UserE);

    const adminRole: RoleE = await roleRepo.save({
        name: RoleTypes.Admin,
      });

    const contributorRole: RoleE = await roleRepo.save({
        name: RoleTypes.Contributor,
      });

    const editorRole: RoleE = await roleRepo.save({
        name: RoleTypes.Editor,
      });

    const firstAdmin: UserE = userRepo.create({
        username: 'admin',
        password: passwordHash.generate('12345678'),
      });

    firstAdmin.roles = [contributorRole, editorRole, adminRole];
    firstAdmin.prefLang = defLanguage;
    firstAdmin.editLanguages = [];
    firstAdmin.profileImg = 'https://i.imgur.com/6036AIQ.png';
    firstAdmin.name = 'Name';
    firstAdmin.surname = 'Surname';
    firstAdmin.email = 'email@email.com';
    firstAdmin.prefLang = defLanguage;
    await userRepo.save(firstAdmin);

    const firstUser: UserE = userRepo.create({
        username: 'pesho',
        password: passwordHash.generate('12345678'),
      });

    firstUser.roles = [contributorRole, editorRole, adminRole];
    firstUser.prefLang = defLanguage;
    firstUser.editLanguages = [];
    firstUser.profileImg = 'https://i.imgur.com/6036AIQ.png';
    firstUser.name = 'Name';
    firstUser.surname = 'Surname';
    firstUser.email = 'email@email.com';
    firstUser.roles = [contributorRole];
    firstUser.prefLang = defLanguage;
    await userRepo.save(firstUser);

    const secondUser: UserE = userRepo.create({
        username: 'misho',
        password: passwordHash.generate('12345678'),
      });

    secondUser.roles = [contributorRole];
    secondUser.prefLang = defLanguage;
    secondUser.editLanguages = [];
    secondUser.profileImg = 'https://i.imgur.com/6036AIQ.png';
    secondUser.name = 'Name';
    secondUser.surname = 'Surname';
    secondUser.email = 'email@email.com';
    firstAdmin.prefLang = defLanguage;
    await userRepo.save(secondUser);
};
