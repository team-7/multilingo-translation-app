import { Connection, Repository } from 'typeorm';
import { languages } from '../../../common/constants/googleLangs.constant';
import { SuppLangE } from '../../entities/supp-lang.entity';

export const seedLanguages = async (connection: Connection): Promise<SuppLangE> => {

    const langRepo: Repository<SuppLangE> = connection.getRepository(SuppLangE);

    // Add all potentially supported languages
    for (const el of languages) {
        await langRepo.save({name: el.name, index: el.language, supported: false});
      }
      // Set English to default
    const foundLanguage = await langRepo.findOne({ where: { name: 'English' } });
    foundLanguage.supported = true;
    const savedLang: SuppLangE = await langRepo.save(foundLanguage);
    return savedLang;
};
