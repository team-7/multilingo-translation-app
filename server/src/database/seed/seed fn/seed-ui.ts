import { Connection, Repository } from 'typeorm';
import { uiPreseed } from '../../../common/constants/ui-content.constant';
import { SuppLangE } from '../../entities/supp-lang.entity';
import { UIComponentE } from '../../entities/ui-element.entity';
import { createTranslation } from './create-translation';

export const seedUI = async (connection: Connection, defaultLanguage: SuppLangE) => {

    const uiRepo: Repository<UIComponentE> = connection.getRepository(UIComponentE);

    for (const value of uiPreseed) {
        await uiRepo.save({ text: value.text, resourseName: value.resourseName });
        await createTranslation(connection, { text: value.text, type: 'ui-component' }, defaultLanguage);
    }
};
