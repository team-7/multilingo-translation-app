import { createConnection } from 'typeorm';
import { ArticleVersionE } from '../entities/article-version.entity';
import { ArticleE } from '../entities/article.entity';
import { RatingE } from '../entities/rating.entity';
import { SuppLangE } from '../entities/supp-lang.entity';
import { TranslationE } from '../entities/translation.entity';

const main = async () => {
  const connection = await createConnection();
  const articlesRepo = connection.getRepository(ArticleE);
  const articleVerRepo = connection.getRepository(ArticleVersionE);
  const translRepo = connection.getRepository(TranslationE);
  const ratingsRepo = connection.getRepository(RatingE);
  const langRepo = connection.getRepository(SuppLangE);

  await articlesRepo.delete({});
  await articleVerRepo.delete({});
  await ratingsRepo.delete({});
  await translRepo.delete({});
  await langRepo.delete({});

  await connection.close();

  console.log(`Data has been cleared successfully`);
};

main().catch(console.log);
