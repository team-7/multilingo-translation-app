import { Connection, createConnection } from 'typeorm';
import { ArticleCategoryE } from '../entities/article-category.entity';
import { SuppLangE } from '../entities/supp-lang.entity';
import { createTranslation } from './seed fn/create-translation';
import { seedLanguages } from './seed fn/seed-languages';
import { seedUI } from './seed fn/seed-ui';
import { seedUsers } from './seed fn/seed-users';

const main = async () => {
    const connection: Connection = await createConnection({
      type: 'mysql',
      host: 'localhost',
      port: 3307,
      username: 'root',
      password: 'root1234',
      database: 'multilingoDB',
      entities: [__dirname + '/../**/*.entity{.ts,.js}'],
      extra: { connectionLimit: 2000 },
  });

    const categoryRepo = connection.getRepository(ArticleCategoryE);

    const defaultLanguage: SuppLangE = await seedLanguages(connection);
    await seedUsers(connection, defaultLanguage);

    // Created first category news
    await categoryRepo.save({ name: 'news' });
    await createTranslation(connection, { text: 'news', type: 'article' }, defaultLanguage);

    // Add ui
    await seedUI(connection, defaultLanguage);

    await connection.close();

    // tslint:disable-next-line: no-console
    console.log(`Data seeded successfully`);
  };

  // tslint:disable-next-line: no-console
main().catch(console.log);
