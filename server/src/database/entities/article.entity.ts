import { Column, CreateDateColumn, Entity, ManyToOne, PrimaryGeneratedColumn, OneToOne, OneToMany } from 'typeorm';
import { SuppLangE } from './supp-lang.entity';
import { CommentE } from './comment.entity';
import { ArticleCategoryE } from './article-category.entity';
import { UserE } from './user.entity';

@Entity('article')
export class ArticleE {

    @PrimaryGeneratedColumn('uuid')
    id: string;

    @CreateDateColumn()
    createdOn: string;

    @Column({ nullable: false, default: false})
    isDeleted: boolean;

    @ManyToOne(type => UserE, user => user.articles)
    user: Promise<UserE>;

    @ManyToOne(type => SuppLangE)
    language: Promise<SuppLangE>;

    @OneToMany(type => CommentE, comments => comments.article)
    public comments: Promise<CommentE[]>;

    @ManyToOne(type => ArticleCategoryE, category => category.articles)
    category: Promise<ArticleCategoryE>;
}
