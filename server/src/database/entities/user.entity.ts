import { Column, CreateDateColumn, Entity, JoinTable, ManyToMany, OneToMany, PrimaryGeneratedColumn, ManyToOne } from 'typeorm';
import { ArticleE } from './article.entity';
import { CommentE } from './comment.entity';
import { ReplyE } from './reply.entity';
import { RatingE } from './rating.entity';
import { RoleE } from './role.entity';
import { SuppLangE } from './supp-lang.entity';

@Entity('users')
export class UserE  {

    @PrimaryGeneratedColumn('uuid')
    public id: string;

    @Column({type: 'nvarchar', nullable: false, unique: true, length: 20})
    public username: string;

    @Column({type: 'nvarchar', default: null, length: 20})
    public name: string;

    @Column({type: 'nvarchar', default: null, length: 20})
    public surname: string;

    @CreateDateColumn({ nullable: false, type: 'timestamp'})
    public createdOn: Date;

    @Column({type: 'nvarchar', nullable: false, length: 500})
    public password: string;

    @Column({type: 'nvarchar', default: null, length: 40})
    public email: string;

    @Column({type: 'integer', default: 0})
    public powerPoints: number;

    @Column({type: 'nvarchar',  default: null})
    public profileImg: string;

    @Column({type: 'boolean', default: false})
    public isDeleted: boolean;

    @Column({type: 'boolean', default: false})
    public isBanned: boolean;

    @Column({ default: null, type: 'timestamp'})
    public endOfBanDate: Date;

    @Column({type: 'nvarchar', default: '', length: 50})
    public banReason: string;

    @ManyToMany(type => RoleE, { eager: true })
    @JoinTable()
    public roles: RoleE[];

    @OneToMany(type => RatingE, rates => rates.user)
    public rates: Promise<RatingE[]>;

    @OneToMany(type => ArticleE, articles => articles.user)
    public articles: Promise<ArticleE[]>;

    @ManyToOne(type => SuppLangE, { eager: true })
    public prefLang: SuppLangE;

    @OneToMany(type => CommentE, comments => comments.user)
    public comments: Promise<CommentE[]>;

    @OneToMany(type => ReplyE, replies => replies.user)
    public replies: Promise<ReplyE[]>;

    @OneToMany(type => SuppLangE, editLanguages => editLanguages.users, { eager: true })
    public editLanguages: SuppLangE[];

}
