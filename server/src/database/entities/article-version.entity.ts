import { Column, CreateDateColumn, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { ArticleE } from './article.entity';
import { UserE } from './user.entity';

@Entity('article_version')
export class ArticleVersionE {

    @PrimaryGeneratedColumn('uuid')
    id: string;

    @Column({nullable: false})
    versionNum: number;

    @Column({ default: true, nullable: false })
    isCurrent: boolean;

    @Column({ length: '100' })
    title: string;

    @Column({ length: '100' })
    subtitle: string;

    @Column({ length: '3000'})
    content: string;

    @CreateDateColumn()
    updatedOn: string;

    @ManyToOne(type => UserE)
    updatedBy: Promise<UserE>;

    @ManyToOne(type => ArticleE)
    article: Promise<ArticleE>;

    @Column()
    image: string;

}
