import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity('ui_entity')
export class UIComponentE {

    @PrimaryGeneratedColumn('uuid')
    id: string;

    @Column()
    resourseName: string;

    @Column()
    text: string;
}
