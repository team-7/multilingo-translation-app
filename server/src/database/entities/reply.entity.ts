import { PrimaryGeneratedColumn, Column, ManyToOne, Entity, CreateDateColumn } from 'typeorm';
import { CommentE } from './comment.entity';
import { UserE } from './user.entity';
@Entity('replies')
export class ReplyE {

    @PrimaryGeneratedColumn('uuid')
    public id: string;

    @Column({ type: 'text' })
    public content: string;

    @CreateDateColumn({ nullable: false, type: 'timestamp' })
    public createdOn: Date;


    @Column({ type: 'boolean', default: false })
    public isDeleted: boolean;

    @ManyToOne(type => CommentE, comment => comment.replies)
    public comment: Promise<CommentE>;

    @ManyToOne(type => UserE, user => user.replies)
    public user: Promise<UserE>;
}
