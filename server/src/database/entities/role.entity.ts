import { Entity, PrimaryGeneratedColumn, Column } from 'typeorm';
import { RoleTypes } from '../../features/roles/enums/role-types.enum';

@Entity('roles')
export class RoleE {

    @PrimaryGeneratedColumn('uuid')
    public id: string;

    @Column({ type: 'enum', enum: RoleTypes, nullable: false })
    public name: string;

    @Column({type: 'boolean', default: false})
    public isDeleted: boolean;

}
