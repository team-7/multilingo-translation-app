import { SuppLangE } from './supp-lang.entity';
import { ArticleE } from './article.entity';
import { PrimaryGeneratedColumn, Column, CreateDateColumn, Entity, OneToMany, ManyToOne } from 'typeorm';
import { ReplyE } from './reply.entity';
import { UserE } from './user.entity';

@Entity('comments')
export class CommentE {

    @PrimaryGeneratedColumn('uuid')
    public id: string;

    @Column({ nullable: false , type: 'text' })
    public content: string;

    @CreateDateColumn({ nullable: false, type: 'timestamp' })
    public createdOn: Date;

    @Column({ default: 0 })
    public likes: number;

    @ManyToOne(type => SuppLangE, language => language.index)
    public language: Promise<SuppLangE>;

    @OneToMany(type => ReplyE, replies => replies.comment)
    public replies: Promise<ReplyE[]>;

    @ManyToOne(type => ArticleE, article => article.comments)
    public article: Promise<ArticleE>;

    @ManyToOne(type => UserE, user => user.comments)
    public user: Promise<UserE>;

}
