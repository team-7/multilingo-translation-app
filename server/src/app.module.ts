import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { CoreModule } from './core/core.module';
import { DatabaseModule } from './database/database.module';
import { ArticlesModule } from './features/articles/articles.module';
import { AuthModule } from './features/auth/auth.module';
import { CategoryModule } from './features/category/category.module';
import { CommentsModule } from './features/comments/comments.module';
import { LanguagesModule } from './features/languages/languages.module';
import { RatingsModule } from './features/ratings/ratings.module';
import { RepliesModule } from './features/replies/replies.module';
import { TranslationModule } from './features/translate/translation.module';
import { UiComponentsModule } from './features/ui-components/ui-components.module';
import { UsersModule } from './features/users/users.module';

@Module({
  imports: [
    DatabaseModule,
    ArticlesModule,
    CoreModule,
    TranslationModule,
    CommentsModule,
    RepliesModule,
    UsersModule,
    AuthModule,
    RatingsModule,
    UiComponentsModule,
    LanguagesModule,
    CategoryModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
