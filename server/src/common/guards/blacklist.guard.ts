import { CanActivate, ExecutionContext, Injectable, UnauthorizedException } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { AuthService } from '../../features/auth/auth.service';

@Injectable()
export class AuthGuardWithBlacklisting extends AuthGuard('jwt')
implements CanActivate {
    public constructor(private readonly authService: AuthService) {
        super();
    }

    public async canActivate(context: ExecutionContext): Promise<boolean> {
        if (!(await super.canActivate(context))) {

            return false;
    }

        const token: string = context.switchToHttp().getRequest().headers.authorization;
        if (!this.authService.isTokenBlacklisted({token})) {

            throw new UnauthorizedException();
        }

        return true;
    }
}
