import { BadRequestException, CanActivate, ExecutionContext, Injectable } from '@nestjs/common';
import { AuthService } from '../../features/auth/auth.service';

@Injectable()
export class LoggedGuard implements CanActivate {
    public constructor(
        private readonly authService: AuthService,
    ) {}

  public async canActivate(context: ExecutionContext): Promise<boolean> {
    const request = context.switchToHttp().getRequest();
    const token: string | undefined = request.headers.authorization;

    if (token === 'Bearer null' && !(await this.authService.isTokenBlacklisted({ token }))) {
        return true;
    }
    throw new BadRequestException('You are already logged in!');
  }
}
