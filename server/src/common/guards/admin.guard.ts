import { BadRequestException, CanActivate, ExecutionContext, Injectable } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { AuthService } from '../../features/auth/auth.service';
import { RoleTypes } from '../../features/roles/enums/role-types.enum';

@Injectable()
export class AdminGuard extends AuthGuard('jwt') implements CanActivate {

  public constructor(private readonly authService: AuthService) {
    super();
  }

  public async canActivate(context: ExecutionContext): Promise<boolean> {
    const request = context.switchToHttp().getRequest();
    const user = request.user;
    if (user && user.roles.includes(RoleTypes.Admin)) {
      return true;
    }
    throw new BadRequestException('You have to be an admin to perform this action');

  }
}
