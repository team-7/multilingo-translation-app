import { BadRequestException, CanActivate, ExecutionContext, Injectable, UnauthorizedException } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { AuthService } from '../../features/auth/auth.service';
import { LoggedUserPayload } from '../../features/auth/models/logged-user-payload.dto';

@Injectable()
export class BanGuard extends AuthGuard('jwt')
  implements CanActivate {
  public constructor(private readonly authService: AuthService) {
    super();
  }

  public async canActivate(context: ExecutionContext): Promise<boolean> {
    if (!(await super.canActivate(context))) {

      return false;
    }

    const request = context.switchToHttp().getRequest();

    if ((request.user as LoggedUserPayload).isBanned) {
      throw new BadRequestException('You cannot access because you have been banned!');
    }

    return true;
  }
}
