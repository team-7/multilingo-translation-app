export const languages = [
    {
        language: 'sq',
        name: 'Albanian',
    },
    {
        language: 'ar',
        name: 'Arabic',
    },
    {
        language: 'az',
        name: 'Azerbaijani',
    },
    {
        language: 'be',
        name: 'Belarusian',
    },
    {
        language: 'bs',
        name: 'Bosnian',
    },
    {
        language: 'bg',
        name: 'Bulgarian',
    },
    {
        language: 'zh-TW',
        name: 'Chinese',
    },
    {
        language: 'hr',
        name: 'Croatian',
    },
    {
        language: 'cs',
        name: 'Czech',
    },
    {
        language: 'da',
        name: 'Danish',
    },
    {
        language: 'nl',
        name: 'Dutch',
    },
    {
        language: 'en',
        name: 'English',
    },
    {
        language: 'et',
        name: 'Estonian',
    },
    {
        language: 'fi',
        name: 'Finnish',
    },
    {
        language: 'fr',
        name: 'French',
    },
    {
        language: 'de',
        name: 'German',
    },
    {
        language: 'el',
        name: 'Greek',
    },
    {
        language: 'hu',
        name: 'Hungarian',
    },
    {
        language: 'is',
        name: 'Icelandic',
    },
    {
        language: 'it',
        name: 'Italian',
    },
    {
        language: 'ja',
        name: 'Japanese',
    },
    {
        language: 'ko',
        name: 'Korean',
    },
    {
        language: 'lv',
        name: 'Latvian',
    },
    {
        language: 'lt',
        name: 'Lithuanian',
    },
    {
        language: 'lb',
        name: 'Luxembourgish',
    },
    {
        language: 'mk',
        name: 'Macedonian',
    },
    {
        language: 'no',
        name: 'Norwegian',
    },
    {
        language: 'pl',
        name: 'Polish',
    },
    {
        language: 'pt',
        name: 'Portuguese',
    },
    {
        language: 'ro',
        name: 'Romanian',
    },
    {
        language: 'ru',
        name: 'Russian',
    },
    {
        language: 'sr',
        name: 'Serbian',
    },
    {
        language: 'sk',
        name: 'Slovak',
    },
    {
        language: 'sl',
        name: 'Slovenian',
    },
    {
        language: 'es',
        name: 'Spanish',
    },
    {
        language: 'sv',
        name: 'Swedish',
    },
    {
        language: 'th',
        name: 'Thai',
    },
    {
        language: 'tr',
        name: 'Turkish',
    },
    {
        language: 'uk',
        name: 'Ukrainian',
    },
    {
        language: 'uz',
        name: 'Uzbek',
    },
];
