export class DetectedLanguageDTO {
  confidence: string;
  language: string;
  input: string;
}
