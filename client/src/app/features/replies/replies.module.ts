import { NgModule } from '@angular/core';
import { SharedModule } from '../../shared/shared.module';
import { FormsModule } from '@angular/forms';
import { AllRepliesComponent } from './all-replies/replies.component';
import { CreateReplyComponent } from './create-replies/create-reply.component';
import { ReplyInfoComponent } from './reply-info/reply-info.component';
import { EditReplyComponent } from './edit-reply/edit-reply.component';
@NgModule({
  imports: [SharedModule, FormsModule ],
  declarations: [
    AllRepliesComponent,
    CreateReplyComponent,
    ReplyInfoComponent,
    EditReplyComponent,
  ],
  providers: [],
  exports: [
    AllRepliesComponent
  ]
})
export class RepliesModule {}
