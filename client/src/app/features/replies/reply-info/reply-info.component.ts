import { EventEmitter, Output, Input, Component, OnInit, OnDestroy } from '@angular/core';
import { ShowUserDTO } from '../../users/models/show-users.dto';
import { ReplyDTO } from '../models/reply.dto';

@Component({
  selector: 'app-reply-info',
  templateUrl: './reply-info.component.html',
  styleUrls: ['./reply-info.component.css']
})
export class ReplyInfoComponent implements OnInit, OnDestroy {

  @Input() comment: ReplyDTO;
  @Input() user: ShowUserDTO;
  @Output() newTextEvent = new EventEmitter<{commentId: string, newText: {content: string}}>();
  public canEdit: boolean;
  public isEditMenuOpen = false;

  constructor() { }

  ngOnInit() {
    this.canEdit = this.user.roles.length > 1 ? true : false;
  }

  ngOnDestroy(): void {
  }

  toggleEditMenu() {
    this.isEditMenuOpen = !this.isEditMenuOpen;
  }

  editComment(newText: {content: string}) {
    const editedComment = {
      commentId: this.comment.id,
      newText
    };
    this.comment.content = newText.content;
    this.newTextEvent.emit(editedComment);
  }

}
