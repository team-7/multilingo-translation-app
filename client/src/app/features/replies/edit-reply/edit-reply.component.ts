import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { TranslationsService } from '../../../core/services/translation.service';
import { ShowUserDTO } from '../../users/models/show-users.dto';

@Component({
  selector: 'app-edit-reply',
  templateUrl: './edit-reply.component.html',
  styleUrls: ['./edit-reply.component.css']
})
export class EditReplyComponent implements OnInit {

  @Input() public replyContentToEdit: string;
  @Input() public user: ShowUserDTO;

  public editReplyForm: FormGroup;

  @Output() closedMenu = new EventEmitter<boolean>();
  @Output() newTextEvent = new EventEmitter<{content: string}>();


  constructor(
    private readonly fb: FormBuilder,
    private readonly transService: TranslationsService,
  ) {}

  ngOnInit() {
    this.editReplyForm = this.fb.group({
      content: [`${this.replyContentToEdit}`,
      Validators.compose([Validators.required, Validators.minLength(1), Validators.maxLength(500)])],
    });
  }

  editComment() {
    this.transService.getAllTranslations('article').subscribe(translations => {
      translations.filter(tr => {
        if (tr.originalText === this.editReplyForm.value && tr.targetLang.index === this.user.prefLang) {
          this.transService.updateTransl(tr.id, this.editReplyForm.value);
        }
      });
    });
    this.closedMenu.emit(false);
    this.newTextEvent.emit(this.editReplyForm.value);
  }

}
