import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ArticlesService } from '../../../core/services/articles.service';
import { DialogService } from '../../../core/services/dialog.service';
import { LanguagesService } from '../../../core/services/languages.service';
import { UIService } from '../../../core/services/ui.service';
import { UIComponentDTO } from '../../../models/ui-component.dto';
import { LanguageConfirmDialogComponent } from '../../../shared/components/language-confirm-dialog/language-confirm-dialog.component';
import { CategoryDTO } from '../../admin-panel/components/admin-settings/categories/models/category.dto';
import { DetectedLanguageDTO } from '../../languages/models/detected-language.dto';
import { LanguageDTO } from '../../languages/models/language.dto';

@Component({
  selector: 'app-create-article',
  templateUrl: './create-article.component.html',
  styleUrls: ['./create-article.component.css']
})
export class CreateArticleComponent implements OnInit {
  public createArticleForm: FormGroup;
  public allLangs: LanguageDTO[];
  public language: string;
  public ui: UIComponentDTO;
  public photo: string = null;

  // Mock category. Will be with request
  public categories: CategoryDTO[];

  @ViewChild('textArea', { read: ElementRef, static: true })
  textArea: ElementRef;

  constructor(
    private readonly formBuilder: FormBuilder,
    private readonly articlesService: ArticlesService,
    private readonly languagesService: LanguagesService,
    private readonly uiService: UIService,
    private readonly dialogService: DialogService,
    private readonly route: ActivatedRoute,
    private readonly router: Router
  ) {}

  ngOnInit() {
    this.route.data.subscribe(data => {
      this.categories = data.categories;
      this.allLangs = data.allLanguages;
      console.log(this.categories);
      console.log(data);


    });

    this.languagesService.displayLanguage$.subscribe((lang: string) => {
      this.language = lang;
    });

    this.uiService.UI$.subscribe((ui: UIComponentDTO) => {
      this.ui = ui;
    });

    this.createArticleForm = this.formBuilder.group({
      title: [
        '',
        [
          Validators.required,
          Validators.minLength(5),
          Validators.maxLength(100),
        ]
      ],
      subtitle: [
        '',
        [
          Validators.required,
          Validators.minLength(5),
          Validators.maxLength(100),
        ]
      ],
      content: [
        '',
        [
          Validators.required,
          Validators.minLength(20),
          Validators.maxLength(3000),
        ]
      ],
      category: [this.categories[0].name]
    });
  }

  public onAddPhoto(event: string) {

    this.photo = event;
  }

  public onRemovePhoto() {
    this.photo = null;
  }

  public onCreateClick() {
    const content = this.createArticleForm.value;
    const photoToGo = ('' + this.photo).slice(22);


    const category = this.categories.find(
      e => e.name === content.category
      );

    this.articlesService
      .detectArticleLang({
        text: content.title + content.subtitle + content.content
      })
      .subscribe((detected: DetectedLanguageDTO) => {
        const confidence = +detected.confidence * 100;

        if (confidence === 100 && this.language === detected.language) {

          this.articlesService
            .createArticle({
              ...content,
              category: category.id,
              language: detected.language,
              image: photoToGo
            })
            .subscribe(data => {
              this.router.navigateByUrl(`articles/${data.article.id}`);
            });
        } else {
          const foundLangIndex = this.allLangs.findIndex(
            e => e.index === detected.language
          );
          const dataObj = { detectedLang: undefined, suppLangs: [] };
          if (confidence >= 60) {
            // We detected that that this article is in this.allLangs[foundLangIndex].name
            dataObj.detectedLang = this.allLangs[foundLangIndex];
            dataObj.suppLangs = this.allLangs.filter(e => e.supported);
          } else {
            dataObj.suppLangs = this.allLangs.filter(e => e.supported);
            dataObj.detectedLang = undefined;
          }

          const dialogRef = this.dialogService.open(
            LanguageConfirmDialogComponent,
            {
              ...dataObj,
              ui: this.ui
            }
          );

          const eventSub = dialogRef.componentInstance.choosenLang.subscribe(
            (lang: LanguageDTO) => {
              console.log(content);

              this.articlesService
                .createArticle({
                  ...content,
                  category: category.id,
                  language: lang.index,
                  image: photoToGo
                })
                .subscribe(data => {
                  this.router.navigateByUrl(`articles/${data.article.id}`);
                  dialogRef.close();
                });
            }
          );

          dialogRef.afterClosed().subscribe(() => {
            eventSub.unsubscribe();
          });
        }
      });
  }
}
