import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { CategoriesService } from '../../../core/services/categories.service';
import { LanguagesService } from '../../../core/services/languages.service';
import { CategoryDTO } from '../../admin-panel/components/admin-settings/categories/models/category.dto';

@Injectable()
export class AllCategoriesResolver implements Resolve<CategoryDTO[]> {
  constructor(
    private readonly categoriesService: CategoriesService,
    private readonly langService: LanguagesService,
    private readonly router: Router
  ) {}

  resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<CategoryDTO[]> {
    let language: string;

    this.langService.displayLanguage$.subscribe((lang: string) => {
      language = lang;
    });

    return this.categoriesService.getAllCategories(language).pipe(
      map((categories: CategoryDTO[]) => {
        if (categories) {
          console.log(categories);

          return categories;
        }
        return;
      })
    );
  }
}
