import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot } from '@angular/router';
import { Observable, zip } from 'rxjs';
import { map } from 'rxjs/operators';
import { ArticlesService } from '../../../core/services/articles.service';
import { AuthService } from '../../../core/services/auth.service';
import { LanguagesService } from '../../../core/services/languages.service';
import { RatingsService } from '../../../core/services/ratings.service';
import { UIService } from '../../../core/services/ui.service';
import { ShowUserDTO } from '../../users/models/show-users.dto';
import { ArticleRatingsDTO } from '../models/article-ratings.dto';
import { ShowArticleVersionDTO } from '../models/show-article-version.dto';

@Injectable()
export class SingleArticleResolver
  implements
    Resolve<{ article: ShowArticleVersionDTO; ratings: ArticleRatingsDTO }> {
  constructor(
    private readonly articleService: ArticlesService,
    private readonly ratingsService: RatingsService,
    private readonly langService: LanguagesService,
    private readonly authService: AuthService,
    private readonly uiService: UIService,
    private readonly router: Router
  ) {}

  resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<{
    article: ShowArticleVersionDTO;
    ratings: ArticleRatingsDTO;
  }> {
    const id = route.paramMap.get('id');
    let lang: string;

    this.langService.displayLanguage$.subscribe((showLang: string) => {
      lang = showLang;
    });

    let loggedUser: ShowUserDTO;
    this.authService.loggedUser$.subscribe((user: ShowUserDTO) => {
      loggedUser = user;
    });

    const data$ = zip(
      this.articleService.getArticleByIdInLang(id, lang),
      this.ratingsService.getArticleTranslationRatings(id, lang, !!loggedUser),
      this.uiService.UI$
    );

    return data$.pipe(
      map(([article, ratings, ui]) => {
        const isTranslated = article.article.language.index !== lang;

        return { article, ratings, isTranslated, language: lang, ui  };
      })
    );
  }
}
