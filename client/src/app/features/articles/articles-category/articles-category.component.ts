import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { AuthService } from '../../../core/services/auth.service';
import { CategoriesService } from '../../../core/services/categories.service';
import { LanguagesService } from '../../../core/services/languages.service';
import { UIService } from '../../../core/services/ui.service';
import { UIComponentDTO } from '../../../models/ui-component.dto';
import { CategoryDTO } from '../../admin-panel/components/admin-settings/categories/models/category.dto';
import { ShowArticleVersionDTO } from '../models/show-article-version.dto';

@Component({
  selector: 'app-articles-category',
  templateUrl: './articles-category.component.html',
  styleUrls: ['./articles-category.component.css']
})
export class ArticlesCategoryComponent implements OnInit, OnDestroy {
  private subLang: Subscription;
  private isLoggedSub: Subscription;

  public ui: UIComponentDTO;
  public displayLanguage: string;
  public articles: ShowArticleVersionDTO[];
  public categories: CategoryDTO[];
  public isLogged: boolean;
  constructor(
    private readonly route: ActivatedRoute,
    private readonly router: Router,
    private readonly languagesService: LanguagesService,
    private readonly uiService: UIService,
    private readonly categoriesService: CategoriesService,
    private readonly authService: AuthService,
  ) {}

  ngOnInit() {
    this.route.data.subscribe(data => {
      this.articles = data.articles.articles;
      this.ui = data.articles.ui;
      this.displayLanguage = data.articles.language;
      this.categories = data.articles.categories;
    });

    this.isLoggedSub = this.authService.isLogged$.subscribe(
      (isLog: boolean) => {
        this.isLogged = isLog;
      }
    );

    this.subLang = this.languagesService.displayLanguage$.subscribe((lang: string) => {

      if (this.displayLanguage !== lang) {
        this.displayLanguage = lang;

        this.categoriesService
          .getAllCategories(lang)
          .subscribe((categories: CategoryDTO[]) => {
            this.categories = categories;
        });

        this.uiService.UI$.subscribe((ui: UIComponentDTO) => {
          this.ui = ui;
        });
      }

    });

  }

  ngOnDestroy() {
      this.subLang.unsubscribe();
  }

  goToCreate() {
    this.router.navigateByUrl(`${this.isLogged ? 'articles/create' : 'landing'}`);
  }

  redirectToArticle(id: string) {
    this.router.navigate(['/articles', id]);
  }
}
