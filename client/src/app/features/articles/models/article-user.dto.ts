export class ArticleUserDTO {
  id: string;
  username: string;
  name: string;
  surname: string;
  powerPoints: number;
  profileImg: string;
}
