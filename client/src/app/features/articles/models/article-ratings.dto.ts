import { ArticlePhraseRatingDTO } from './article-phrase-rating.dto';

export class ArticleRatingsDTO {
  avgRating: number;
  title: ArticlePhraseRatingDTO;
  subtitle: ArticlePhraseRatingDTO;
  content: ArticlePhraseRatingDTO;
}
