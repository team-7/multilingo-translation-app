export class EditArticleDTO {
  title: string;
  subtitle: string;
  content: string;
  category: string;
  image: string;
}
