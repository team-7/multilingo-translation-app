export class ArticlePhraseRatingDTO {
  avg: number;
  sum: number;
  count: number;

  myRate: number | undefined;
}
