import { ArticleInfoDTO } from './article-info.dto';
import { ArticleRatingsDTO } from './article-ratings.dto';

export class ShowArticleVersionDTO {
  id: string;
  versionNum: number;
  title: string;
  subtitle: string;
  content: string;
  updatedOn: string;
  isCurrent: boolean;
  article: ArticleInfoDTO;
  ratings: ArticleRatingsDTO;
  image: string;
}
