import { CommonModule } from '@angular/common';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { NgxImageCompressService } from 'ngx-image-compress';
import { ToastrModule } from 'ngx-toastr';
import { of } from 'rxjs';
import { ArticlesService } from '../../../core/services/articles.service';
import { AuthService } from '../../../core/services/auth.service';
import { DialogService } from '../../../core/services/dialog.service';
import { LanguagesService } from '../../../core/services/languages.service';
import { RatingsService } from '../../../core/services/ratings.service';
import { UIService } from '../../../core/services/ui.service';
import { UIComponentDTO } from '../../../models/ui-component.dto';
import { SharedModule } from '../../../shared/shared.module';
import { AllCommentsComponent } from '../../comments/all-comments/comments.component';
import { CommentInfoComponent } from '../../comments/comment-info/comment-info.component';
import { CreateCommentComponent } from '../../comments/create-comment/create-comment.component';
import { EditCommentComponent } from '../../comments/edit-comment/edit-comment.component';
import { LanguageDTO } from '../../languages/models/language.dto';
import { ShowUserDTO } from '../../users/models/show-users.dto';
import { ArticleRatingsComponent } from '../article-ratings/article-ratings.component';
import { EditArticleDialogComponent } from '../edit-article-dialog/edit-article-dialog.component';
import { ArticleInfoDTO } from '../models/article-info.dto';
import { ArticleRatingsDTO } from '../models/article-ratings.dto';
import { ArticleUserDTO } from '../models/article-user.dto';
import { ArticleVersionDTO } from '../models/article-version.dto';
import { ShowArticleVersionDTO } from '../models/show-article-version.dto';
import { RevertArticleVersionComponent } from '../revert-article-version/revert-article-version.component';
import { SingleArticleComponent } from './single-article.component';

describe('SingleArticleComponent', () => {
  let route;
  let dialogService;
  let articlesService;
  let ratingsService;
  let languagesService;
  let uiService;
  let router;
  let authService;
  let imageCompress;

  let fixture: ComponentFixture<SingleArticleComponent>;
  let component: SingleArticleComponent;

  beforeEach((async () => {
    jest.clearAllMocks();

    dialogService = {
      open() {},
    };

    articlesService = {
      getArticleByIdInLang() {},
      getOriginalArticle() {},
      updateArticle() {},
      getAllArticleVersions() {},
    };

    ratingsService = {
      getArticleTranslationRatings() {},
      rateTranslation() {},
    };

    languagesService = {
      displayLanguage$: of(),
    };

    uiService = {
      UI$: of(),
    };

    router = {
      navigateByUrl() {},
    };

    authService = {
      loggedUser$: of(),
    };

    route = {
      data: of({
        article: {
          article: {},
          ratings: {},
        }
      })
    };

    imageCompress = {
      compressFile: () => {},
    };

    TestBed.configureTestingModule({
      imports: [
        ToastrModule.forRoot(),
          RouterTestingModule,
          BrowserAnimationsModule,
          SharedModule,
          CommonModule,
      ],
      declarations: [
          SingleArticleComponent,
          EditArticleDialogComponent,
          RevertArticleVersionComponent,
          ArticleRatingsComponent,
          AllCommentsComponent,
          CommentInfoComponent,
          CreateCommentComponent,
          EditCommentComponent,
      ],
      providers: [
        DialogService,
        ArticlesService,
        RatingsService,
        LanguagesService,
        UIService,
        AuthService,
        NgxImageCompressService,
      ],

  })
      .overrideProvider(DialogService, { useValue: dialogService })
      .overrideProvider(ArticlesService, { useValue: articlesService })
      .overrideProvider(RatingsService, { useValue: ratingsService })
      .overrideProvider(AuthService, { useValue: authService })
      .overrideProvider(ActivatedRoute, { useValue: route })
      .overrideProvider(LanguagesService, { useValue: languagesService })
      .overrideProvider(UIService, { useValue: uiService })
      .overrideProvider(NgxImageCompressService, { useValue: imageCompress })
      .compileComponents();

    fixture = TestBed.createComponent(SingleArticleComponent);
    component = fixture.componentInstance;
  }));

  it('should be defined', () => {
    expect(component).toBeDefined();
  });

  describe('ngOnInit should', () => {

    it('init correctly with the route data', () => {
      const articleToShow = new ShowArticleVersionDTO();
      articleToShow.article = new ArticleInfoDTO();
      articleToShow.article.user = new ArticleUserDTO();
      articleToShow.article.language = new LanguageDTO();
      const ratings = new ArticleRatingsDTO();
      const isTranslated = false;
      const article = {article: articleToShow, ratings, isTranslated};
      route.data = of({article});

      component.ngOnInit();

      expect(component.article).toBe(articleToShow);
      expect(component.ratings).toBe(ratings);

    });
  });

  it('initialize correctly subject subs', () => {
    const user = new ShowUserDTO();
    const ui = new UIComponentDTO();

    authService.loggedUser$ = of(user);
    uiService.UI$ = of(ui);

    component.ngOnInit();

    expect(component.loggedUser).toBe(user);
    expect(component.ui).toBe(ui);
  });

  it('initialize correctly when displayLanguage !== article language', () => {
      const lang = 'en';
      const article = new ShowArticleVersionDTO();
      article.article = new ArticleInfoDTO();
      article.article.id = '1';
      article.article.language = new LanguageDTO();
      article.article.language.index = 'bg';

      const ratings = new ArticleRatingsDTO();

      route.data = of({ article: {article, ratings: null, isTranslated: false }});
      languagesService.displayLanguage$ = of(lang);

      const getArticleSpy = jest.spyOn(articlesService, 'getArticleByIdInLang')
      .mockImplementation(() => of(article) );

      const getRatingsSpy = jest.spyOn(ratingsService, 'getArticleTranslationRatings')
        .mockImplementation(() => of(ratings) );

      // Act
      component.ngOnInit();

      expect(component.article.article.language.index).not.toBe(component.displayLanguage);
      expect(getRatingsSpy).toBeCalledTimes(1);
      expect(getRatingsSpy).toHaveBeenCalledWith('1', 'en', false);
      expect(component.ratings).toBe(ratings);
      expect(component.isArticleTranslated).toBe(true);
  });

  it('initialize correctly when displayLanguage === article language', () => {
    const lang = 'en';
    const article = new ShowArticleVersionDTO();
    article.article = new ArticleInfoDTO();
    article.article.id = '1';
    article.article.language = new LanguageDTO();
    article.article.language.index = 'en';
    const ratings = new ArticleRatingsDTO();

    component.isArticleTranslated = false;
    languagesService.displayLanguage$ = of(lang);
    route.data = of({ article: { article }});
    const getArticleSpy = jest.spyOn(articlesService, 'getArticleByIdInLang')
    .mockImplementation(() => of(article) );

    const getRatingsSpy = jest.spyOn(ratingsService, 'getArticleTranslationRatings')
    .mockImplementation(() => of(ratings) );

    // Act
    component.ngOnInit();

    expect(getArticleSpy).toBeCalledTimes(1);
    expect(getArticleSpy).toHaveBeenCalledWith('1', lang);
    expect(component.article).toBe(article);
    expect(component.article).not.toBe(component.displayLanguage);
    expect(getRatingsSpy).toBeCalledTimes(0);
    expect(component.ratings).toBe(null);
    expect(component.isArticleTranslated).toBe(false);

});

  describe('ngOnDestroy should', () => {
    it('Close all subs', () => {
      const user = new ShowUserDTO();
      const ui = new UIComponentDTO();

      authService.loggedUser$ = of(user);
      uiService.UI$ = of(ui);
      languagesService.displayLanguage$ = of('');

      component.ngOnInit();
      component.ngOnDestroy();

      expect((component as any).userSub.closed).toBe(true);
      expect((component as any).langSub.closed).toBe(true);
      expect((component as any).uiSub.closed).toBe(true);
    });
  });

  describe('openEditDialog should', () => {

    it('call articles service', () => {
      const article = new ShowArticleVersionDTO();
      article.article = new ArticleInfoDTO();
      article.article.id = '1';

      component.article = article;

      const getOrigArticleSpy = jest.spyOn(articlesService, 'getOriginalArticle')
      .mockImplementation(() => of({}));

      component.openEditDialog();

      expect(getOrigArticleSpy).toHaveBeenCalledTimes(1);
      expect(getOrigArticleSpy).toHaveBeenCalledWith('1');

    });

    it('open EditArticleDialogComponent with correct parameter', () => {
      const articleToEdit = new ShowArticleVersionDTO();
      const article = new ShowArticleVersionDTO();
      article.article = new ArticleInfoDTO();
      component.ui = {};
      component.article = article;
      jest.spyOn(articlesService, 'getOriginalArticle')
      .mockImplementation(() => of(articleToEdit));

      const openDialogSpy = jest.spyOn(dialogService, 'open');

      component.openEditDialog();

      expect(openDialogSpy).toHaveBeenCalledTimes(1);
      expect(openDialogSpy).toHaveBeenCalledWith(EditArticleDialogComponent, { article: {}, ui: {} });
    });

  });

  describe('onRevertClick should', () => {

    beforeEach(() => {
      const article = new ShowArticleVersionDTO();
      article.article = new ArticleInfoDTO();
      article.article.id = '1';

      component.article = article;
    });
    it('call getAllArticleVersions with correct param if there are no loaded versions', () => {

      const allVersionsSpy = jest.spyOn(articlesService, 'getAllArticleVersions')
        .mockImplementation(() => of());

      component.onRevertClick();

      expect(allVersionsSpy).toHaveBeenCalledTimes(1);
      expect(allVersionsSpy).toHaveBeenCalledWith('1');
    });

    it('set versions and call openRevertDialog', () => {

      jest.spyOn(articlesService, 'getAllArticleVersions')
        .mockImplementation(() => of([new ArticleVersionDTO(), new ArticleVersionDTO()]));

      component.onRevertClick();

      expect(component.versions).toEqual([{}, {}]);
    });

    it('do not call getAllArticleVersions if versions are present', () => {
      component.versions = [new ArticleVersionDTO(), new ArticleVersionDTO()];
      component.ui = {};
      const getAllArticleVersionsSpy = jest.spyOn(articlesService, 'getAllArticleVersions')
        .mockImplementation(() => of([new ArticleVersionDTO()]));
      const openDialog = jest.spyOn(dialogService, 'open')
      .mockImplementation(() => ({
        componentInstance: {
          revertArticle: of(new ArticleVersionDTO()),
        },
        afterClosed: () => of(true),
      }));


      component.onRevertClick();

      expect(getAllArticleVersionsSpy).toHaveBeenCalledTimes(0);
      expect(openDialog).toHaveBeenCalledTimes(1);
      expect(openDialog).toHaveBeenCalledWith(RevertArticleVersionComponent, { verses: [{}, {}], ui: {}});
    });
  });

  describe('rateTextFromArticle should', () => {

    let article: ShowArticleVersionDTO;
    let event: {rate: number, articleComponent: string, initRate: number | undefined};

    let text: string;
    let originalLang: string;
    let toLang: string;
    let rate: number;

    beforeEach(() => {
      article = {
        ...(new ShowArticleVersionDTO()),
        title: 'New title',
        subtitle: 'New subtitle',
        content: 'New Content',
        article:  {
          ...(new ArticleInfoDTO()),
          language: {
            ...(new LanguageDTO()),
            index: 'en',
          }
        }
      };

      text = 'Something';
      originalLang = 'en';
      toLang = 'bg';
      rate = 5;

      component.article = article;
     });

    it('call rateTranslation with the right params', () => {

      const spy = jest.spyOn(ratingsService, 'rateTranslation')
        .mockImplementation(() => of({ rate: 5}));
      component.displayLanguage = 'bg';

      component.rateTextFromArticle({rate: 5, articleComponent: 'title', initRate: 2 });

      expect(spy).toHaveBeenCalledTimes(1);
      expect(spy).toHaveBeenCalledWith({
        text: 'New title',
        originalLang: 'en',
        toLang: 'bg',
        rate: 5,
      });
     });

    it('call rateTranslations and do the math', () => {
      const spy = jest.spyOn(ratingsService, 'rateTranslation')
      .mockImplementation(() => of({ rate: 5 }));

      component.ratings = {
        avgRating: 0,
        title: {
          avg: 0,
          sum: 0,
          count: 0,
          myRate: 0,
        },
        subtitle: {
          avg: 0,
          sum: 0,
          count: 0,
          myRate: 0,
        },
        content: {
          avg: 0,
          sum: 0,
          count: 0,
          myRate: 0,
        }
      };
      component.displayLanguage = 'bg';

      component.rateTextFromArticle({rate: 5, articleComponent: 'title', initRate: undefined });

      expect(component.ratings.title).toEqual({ avg: 5, sum: 5, count: 1, myRate: 5});
      expect(component.ratings.avgRating).toBe(5 / 3);
     });

  });

});
