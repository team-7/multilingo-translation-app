import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription, zip } from 'rxjs';
import { takeLast } from 'rxjs/operators';
import { ArticlesService } from '../../../core/services/articles.service';
import { AuthService } from '../../../core/services/auth.service';
import { CategoriesService } from '../../../core/services/categories.service';
import { DialogService } from '../../../core/services/dialog.service';
import { LanguagesService } from '../../../core/services/languages.service';
import { RatingsService } from '../../../core/services/ratings.service';
import { UIService } from '../../../core/services/ui.service';
import { UIComponentDTO } from '../../../models/ui-component.dto';
import { LoggedUserDTO } from '../../users/models/logged-user.dto';
import { EditArticleDialogComponent } from '../edit-article-dialog/edit-article-dialog.component';
import { ArticleRatingsDTO } from '../models/article-ratings.dto';
import { ArticleUserDTO } from '../models/article-user.dto';
import { ArticleVersionDTO } from '../models/article-version.dto';
import { EditArticleDTO } from '../models/edit-article.dto';
import { ShowArticleVersionDTO } from '../models/show-article-version.dto';
import { RevertArticleVersionComponent } from '../revert-article-version/revert-article-version.component';

@Component({
  selector: 'app-single-article',
  templateUrl: './single-article.component.html',
  styleUrls: ['./single-article.component.css']
})
export class SingleArticleComponent implements OnInit, OnDestroy {
  private uiSub: Subscription;
  private langSub: Subscription;
  private userSub: Subscription;

  public displayLanguage: string;
  public ui: UIComponentDTO;

  public loggedUser: LoggedUserDTO;
  public article: ShowArticleVersionDTO;
  public author: ArticleUserDTO;

  public ratings: ArticleRatingsDTO;
  public versions: ArticleVersionDTO[];

  public isArticleTranslated: boolean;

  constructor(
    private readonly route: ActivatedRoute,
    private readonly dialogService: DialogService,
    private readonly articlesService: ArticlesService,
    private readonly ratingsService: RatingsService,
    private readonly languagesService: LanguagesService,
    private readonly uiService: UIService,
    private readonly router: Router,
    private readonly authService: AuthService,
    private readonly categoriesService: CategoriesService,
  ) {}

  ngOnInit() {
    this.route.data.subscribe(({ article }) => {
      this.article = article.article;
      this.ratings = article.ratings;
      this.isArticleTranslated = article.isTranslated;
      this.displayLanguage = article.language;
      this.ui = article.ui;
      console.log(this.displayLanguage);

      this.author = this.article.article.user;
    });

    this.userSub = this.authService.loggedUser$.subscribe(
      (loggedUser: LoggedUserDTO) => {
        this.loggedUser = loggedUser;
      }
    );

    this.langSub = this.languagesService.displayLanguage$.subscribe(
      (lang: string) => {
        if (lang !== this.displayLanguage) {

          this.uiService.UI$.subscribe((ui: UIComponentDTO) => {

            this.ui = ui;
          });

          this.displayLanguage = lang;
          this.articlesService
            .getArticleByIdInLang(this.article.article.id, lang)
            .subscribe((article: ShowArticleVersionDTO) => {

              this.article = article;
            });


          if (this.displayLanguage !== this.article.article.language.index) {
                this.ratingsService
                  .getArticleTranslationRatings(
                    this.article.article.id,
                    this.displayLanguage,
                    !!this.loggedUser
                  )
                  .subscribe(ratings => {

                    this.ratings = ratings;
                    this.isArticleTranslated = true;
                  });
              } else {
                console.log('why');

                this.isArticleTranslated = false;
                this.ratings = null;
              }

        }
    });

    // this.uiSub = this.uiService.UI$.subscribe((ui: UIComponentDTO) => {

    //   this.ui = ui;
    // });
  }

  // Open with original article
  openEditDialog() {

    const data$ = zip(
      this.articlesService.getOriginalArticle(this.article.article.id),
      this.categoriesService.getAllCategories(this.article.article.language.index),
    );

    data$.subscribe(
      ([article, categories]) => {
        console.log(categories);

        const dialogRef = this.dialogService.open(EditArticleDialogComponent, {
          article,
          ui: this.ui,
          categories,
        });
        const updateEvent = dialogRef.componentInstance.update.subscribe(
          (updateContent: EditArticleDTO) => {
            if (updateContent.image !== this.article.image) {
              updateContent.image = updateContent.image.slice(22);
            }

            this.articlesService
              .updateArticle(this.article.article.id, {
                ...updateContent
              })
              .subscribe(
                _ => {
                  this.articlesService
                    .getArticleByIdInLang(
                      this.article.article.id,
                      this.displayLanguage
                    )
                    .subscribe(latestArticle => {
                      this.article = latestArticle;
                      if (
                        latestArticle.article.language.index !==
                        this.displayLanguage
                      ) {
                        this.ratingsService
                          .getArticleTranslationRatings(
                            latestArticle.article.id,
                            this.displayLanguage,
                            !!this.loggedUser
                          )
                          .subscribe((ratings: ArticleRatingsDTO) => {
                            this.ratings = ratings;
                            this.isArticleTranslated = true;

                            dialogRef.close();
                          });
                      } else {
                        this.isArticleTranslated = false;
                        dialogRef.close();
                      }
                    });
                },
                error => {
                  // Hardcoded for now till 500 page is made or notify the user his version already exists
                  console.log(error);
                  // this.router.navigateByUrl(`articles?lang=en`);
                  dialogRef.close();
                }
              );
          }
        );

        dialogRef.afterClosed().subscribe(() => {
          updateEvent.unsubscribe();
        });

      }
    );

    // this.articlesService
    //   .getOriginalArticle(this.article.article.id)
    //   .subscribe((articleToEdit: ShowArticleVersionDTO) => {
    //     const dialogRef = this.dialogService.open(EditArticleDialogComponent, {
    //       article: articleToEdit,
    //       ui: this.ui
    //     });
    //     const updateEvent = dialogRef.componentInstance.update.subscribe(
    //       (updateContent: EditArticleDTO) => {
    //         if (updateContent.image !== this.article.image) {
    //           updateContent.image = updateContent.image.slice(22);
    //         }

    //         this.articlesService
    //           .updateArticle(this.article.article.id, {
    //             ...updateContent
    //           })
    //           .subscribe(
    //             _ => {
    //               this.articlesService
    //                 .getArticleByIdInLang(
    //                   this.article.article.id,
    //                   this.displayLanguage
    //                 )
    //                 .subscribe(latestArticle => {
    //                   this.article = latestArticle;
    //                   if (
    //                     latestArticle.article.language.index !==
    //                     this.displayLanguage
    //                   ) {
    //                     this.ratingsService
    //                       .getArticleTranslationRatings(
    //                         latestArticle.article.id,
    //                         this.displayLanguage,
    //                         !!this.loggedUser
    //                       )
    //                       .subscribe((ratings: ArticleRatingsDTO) => {
    //                         this.ratings = ratings;
    //                         this.isArticleTranslated = true;

    //                         dialogRef.close();
    //                       });
    //                   } else {
    //                     this.isArticleTranslated = false;
    //                     dialogRef.close();
    //                   }
    //                 });
    //             },
    //             error => {
    //               // Hardcoded for now till 500 page is made or notify the user his version already exists
    //               console.log(error);
    //               // this.router.navigateByUrl(`articles?lang=en`);
    //               dialogRef.close();
    //             }
    //           );
    //       }
    //     );

    //     dialogRef.afterClosed().subscribe(() => {
    //       updateEvent.unsubscribe();
    //     });
    //   });
  }

  public onRevertClick() {
    if (!this.versions) {
      this.articlesService
        .getAllArticleVersions(this.article.article.id)
        .subscribe((articleVersions: ArticleVersionDTO[]) => {
          this.versions = articleVersions;
          this.openRevertDialog(this.versions);
        });
    } else {
      this.openRevertDialog(this.versions);
    }
  }

  private openRevertDialog(versions: ArticleVersionDTO[]) {
    const dialogRef = this.dialogService.open(RevertArticleVersionComponent, {
      verses: this.versions.sort((a, b) => a.versionNum - b.versionNum),
      ui: this.ui
    });
    const revertArticleEvent: Subscription = dialogRef.componentInstance.revertArticle.subscribe(
      (revVersion: ArticleVersionDTO) => {
        this.articlesService
          .revertArticle(this.article.article.id, { versionId: revVersion.id })
          .subscribe((reverted: ArticleVersionDTO) => {
            this.versions[
              this.versions.findIndex((e: ArticleVersionDTO) => e.isCurrent)
            ].isCurrent = false;
            this.versions[
              this.versions.findIndex(
                (e: ArticleVersionDTO) => e.id === reverted.id
              )
            ].isCurrent = true;
            dialogRef.close();
          });
      }
    );
    dialogRef.afterClosed().subscribe(() => {
      revertArticleEvent.unsubscribe();
      this.articlesService
        .getArticleByIdInLang(this.article.article.id, this.displayLanguage)
        .pipe(takeLast(1))
        .subscribe((revertedArticleInLang: ShowArticleVersionDTO) => {
          this.article = revertedArticleInLang;
        });
    });
  }

  public rateTextFromArticle(event: {
    rate: number;
    articleComponent: string;
    initRate: number | undefined;
  }) {
    let text: string;

    const lang: string = this.article.article.language.index;

    text = this.article[event.articleComponent];

    const ratingSub: Subscription = this.ratingsService
      .rateTranslation({
        text,
        originalLang: lang,
        toLang: this.displayLanguage,
        rate: event.rate
      })
      .subscribe(({ rate }) => {
        if (event.initRate) {
          this.ratings[event.articleComponent].sum =
            +this.ratings[event.articleComponent].sum +
            (+event.rate - +event.initRate);
          this.ratings[event.articleComponent].avg =
            +this.ratings[event.articleComponent].sum /
            +this.ratings[event.articleComponent].count;
        } else {
          this.ratings[event.articleComponent].count++;
          this.ratings[event.articleComponent].sum =
            +this.ratings[event.articleComponent].sum + +rate;
          this.ratings[event.articleComponent].avg =
            +this.ratings[event.articleComponent].sum /
            +this.ratings[event.articleComponent].count;
        }
        this.ratings[event.articleComponent].myRate = +event.rate;

        let sum = 0;
        let count = 0;
        for (const key of Object.keys(this.ratings)) {
          if (key === 'avgRating') {
            continue;
          }
          sum += +this.ratings[key].avg;
          count++;
        }
        this.ratings.avgRating = sum / count;
        ratingSub.unsubscribe();
      });
  }

  public getAuthorNameAndSurname(): string {
    return (
      this.article.article.user.name + ' ' + this.article.article.user.surname
    );
  }

  public goToAuthorProfile() {
    this.router.navigateByUrl(`users/${this.author.id}`);
  }

  public ngOnDestroy() {
    if (this.uiSub) {
      this.uiSub.unsubscribe();
    }

    if (this.userSub) {
      this.userSub.unsubscribe();
    }

    if (this.langSub) {
      this.langSub.unsubscribe();
    }
  }

  deleteArticle() {
    this.articlesService
      .deleteArticle(this.article.article.id)
      .subscribe((result: { msg: string }) => {
        this.router.navigateByUrl('/home');
      });
  }
}
