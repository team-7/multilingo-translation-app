import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { UIComponentDTO } from '../../../models/ui-component.dto';

@Component({
  selector: 'app-upload-image',
  templateUrl: './upload-image.component.html',
  styleUrls: ['./upload-image.component.css']
})
export class UploadImageComponent implements OnInit {
  @Input() public previewUrl = undefined;
  @Input() ui: UIComponentDTO;
  @Output() public addPhoto: EventEmitter<File> = new EventEmitter();
  @Output() public removePhoto: EventEmitter<void> = new EventEmitter();

  constructor() {}

  ngOnInit() {}

  photoInputChange(event) {
    const photo: File = event.target.files[0];
    // Show preview
    const mimeType = photo.type;
    if (mimeType.match(/image\/*/) == null) {
      return;
    }

    const reader = new FileReader();
    reader.readAsDataURL(photo);
    reader.onload = _ => {
      this.previewUrl = reader.result;
      this.addPhoto.emit(this.previewUrl);
    };
  }

  removeImg() {
    this.previewUrl = undefined;
    this.removePhoto.emit();
  }
}
