import { Component, EventEmitter, Inject, OnInit, Output } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { UIComponentDTO } from '../../../models/ui-component.dto';
import { EditArticleDialogComponent } from '../edit-article-dialog/edit-article-dialog.component';
import { ArticleVersionDTO } from '../models/article-version.dto';

@Component({
  selector: 'app-revert-article-version',
  templateUrl: './revert-article-version.component.html',
  styleUrls: ['./revert-article-version.component.css']
})
export class RevertArticleVersionComponent implements OnInit {
  @Output() revertArticle: EventEmitter<ArticleVersionDTO> = new EventEmitter();
  public ui: UIComponentDTO;
  constructor(
    public dialogRef: MatDialogRef<EditArticleDialogComponent>,
    @Inject(MAT_DIALOG_DATA)
    public data: { verses: ArticleVersionDTO[]; ui: UIComponentDTO }
  ) {}

  ngOnInit() {
    this.ui = this.data.ui;
  }

  onNoClick() {
    this.dialogRef.close();
  }

  onSubmit(version: ArticleVersionDTO) {
    this.revertArticle.emit(version);
  }
}
