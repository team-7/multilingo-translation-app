import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { ArticlesService } from '../../../core/services/articles.service';
import { CategoriesService } from '../../../core/services/categories.service';
import { LanguagesService } from '../../../core/services/languages.service';
import { UIService } from '../../../core/services/ui.service';
import { UIComponentDTO } from '../../../models/ui-component.dto';
import { CategoryDTO } from '../../admin-panel/components/admin-settings/categories/models/category.dto';
import { ShowArticleVersionDTO } from '../models/show-article-version.dto';

@Component({
  selector: 'app-all-articles',
  templateUrl: './all-articles.component.html',
  styleUrls: ['./all-articles.component.css']
})
export class AllArticlesComponent implements OnInit, OnDestroy {
  public language: string;
  public articles: ShowArticleVersionDTO[];
  public ui: UIComponentDTO;
  public categories: CategoryDTO[];

  private readonly subs: Subscription[] = [];

  constructor(
    private readonly uiService: UIService,
    private readonly languageService: LanguagesService,
    private readonly route: ActivatedRoute,
    private readonly router: Router,
    private readonly articlesService: ArticlesService,
    private readonly categoriesService: CategoriesService
  ) {}

  ngOnInit() {
    this.route.data.subscribe(data => {

      this.articles = data.data.articles;
      this.ui = data.data.ui;
      this.language = data.data.language;
      this.categories = data.data.categories;
    });

    const langSub: Subscription = this.languageService.displayLanguage$.subscribe(
      (lang: string) => {

        if (this.language !== lang) {
          this.language = lang;

          this.articlesService
          .getAllArticles(lang)
          .subscribe((articles: ShowArticleVersionDTO[]) => {

            this.articles = articles;
          });

          const uiSub = this.uiService.UI$.subscribe(
            (ui: UIComponentDTO) => {

              this.ui = ui;
            }, () => {},
            () => {
            }
          );
          uiSub.unsubscribe();

          this.categoriesService
          .getAllCategories(lang)
          .subscribe((categories: CategoryDTO[]) => {

            this.categories = categories;
          });
        }

      }
    );

    this.subs.push(langSub);
  }

  redirectToArticle(id: string) {
    this.router.navigateByUrl(`/articles/${id}`);
  }

  ngOnDestroy() {
    this.subs.map(e => e.unsubscribe());
  }
}
