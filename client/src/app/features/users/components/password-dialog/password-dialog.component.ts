import { Component, OnInit, EventEmitter, Output, Input, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { LoggedUserDTO } from '../../models/logged-user.dto';
import { LoginCredentialsDTO } from '../../models/login-credential.dto';

@Component({
    selector: 'app-pasword-dialog',
    templateUrl: './password-dialog.component.html',
    styleUrls: ['./password-dialog.component.css']
})
export class PaswordDialogComponent implements OnInit {

    public passwordForm: FormGroup;
    @Output()
    public loginEvent = new EventEmitter<{username: string, password: string}>();

    constructor(
        public dialogRef: MatDialogRef<PaswordDialogComponent>,
        private readonly formBuilder: FormBuilder,
        @Inject(MAT_DIALOG_DATA) public loggedUser: LoggedUserDTO,
    ) {}

    ngOnInit(): void {
        this.passwordForm = this.formBuilder.group({
            password: ['', [Validators.required]],
        });
    }

    onCancel(): void {
        this.dialogRef.close();
    }

    onSubmit(): void {
        const credentials: LoginCredentialsDTO = {
            username: this.loggedUser.username,
            password: this.passwordForm.value.password,
        };
        this.loginEvent.emit(credentials);
        this.dialogRef.close();
    }

}
