import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { ShowUserDTO } from '../../models/show-users.dto';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { UIComponentDTO } from '../../../../models/ui-component.dto';

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.css']
})
export class AboutComponent implements OnInit {

  @Input()
  public user: ShowUserDTO;
  public aboutMenu = false;

  public aboutForm: FormGroup;
  @Input()
  public isOwnerOrAdmin: boolean;

  @Output()
  public updateAboutEvent = new EventEmitter<{name: string, surname: string, email: string}>();
  @Input()
  public ui: UIComponentDTO;

  constructor(
    private readonly formBuilder: FormBuilder,
  ) { }

  ngOnInit() {
    this.aboutForm = this.formBuilder.group({
      name: [`${this.user.name}`, Validators.compose([Validators.required, Validators.minLength(3), Validators.maxLength(20)])],
      surname: [`${this.user.surname}`, Validators.compose([Validators.required, Validators.minLength(3), Validators.maxLength(20)])],
      email: [`${this.user.email}`, Validators.compose([Validators.required, Validators.minLength(5), Validators.maxLength(40)])],
    });
  }

  public updateAbout() {
    this.updateAboutEvent.emit(this.aboutForm.value);

    this.user = {
      ...this.user,
      ...this.aboutForm.value
    };
    this.aboutMenu = !this.aboutMenu;
  }

  public toggleAboutMenu() {
    this.aboutMenu = !this.aboutMenu;
  }


}
