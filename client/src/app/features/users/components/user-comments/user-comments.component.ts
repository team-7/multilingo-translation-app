import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { MatTableDataSource, MatPaginator, MatSort } from '@angular/material';
import { ShowUserDTO } from '../../models/show-users.dto';
import { UIComponentDTO } from '../../../../models/ui-component.dto';
import { CommentDTO } from '../../../comments/models/comment.dto';
import { UsersService } from '../../../../core/services/users.service';
import { NotificationService } from '../../../../core/services/notification.service';

@Component({
  selector: 'app-user-comments',
  templateUrl: './user-comments.component.html',
  styleUrls: ['./user-comments.component.css']
})
export class UserCommentsComponent implements OnInit {

  public comments: CommentDTO[];
  public filteredComments: CommentDTO[];
  @Input()
  public user: ShowUserDTO;

  displayedColumns: string[] = ['content', 'createdOn'];
  dataSource: MatTableDataSource<CommentDTO>;

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;

  @Input()
  public ui: UIComponentDTO;

  @Input()
  public isOwnerOrAdmin: boolean;

  constructor(
    private readonly usersService: UsersService,
    private readonly notifiationsService: NotificationService,
  ) {
  }

  ngOnInit() {
    this.usersService.getUserComments(this.user.id).subscribe(
      (comments) => {
        this.comments = comments;
        this.filteredComments = this.comments;
        const commentsSource = [ ...this.filteredComments ];
        commentsSource.map((val, index) => {
        // tslint:disable-next-line: no-string-literal
        val['tableId'] = index + 1;
        });
        this.dataSource = new MatTableDataSource(commentsSource);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      },
      (err) => {
        this.notifiationsService.error(err.error.message);
      }
    );
  }

  public searchComments(filterValue: string) {
    this.filteredComments = this.comments;
    this.filteredComments = this.filteredComments.filter((comment) => {
      if (comment.content.toLowerCase().includes(filterValue.trim().toLowerCase())) {
        return comment;
      }
    });
    this.dataSource.data = this.filteredComments;

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

}
