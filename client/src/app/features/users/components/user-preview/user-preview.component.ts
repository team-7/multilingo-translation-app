import { Component, OnInit, Input } from '@angular/core';
import { ShowUserDTO } from '../../models/show-users.dto';
import { UIComponentDTO } from '../../../../models/ui-component.dto';

@Component({
  selector: 'app-user-preview',
  templateUrl: './user-preview.component.html',
  styleUrls: ['./user-preview.component.css']
})
export class UserPreviewComponent implements OnInit {

  @Input()
  public user: ShowUserDTO;

  @Input()
  public ui: UIComponentDTO;

  constructor() { }

  ngOnInit() {
  }

}
