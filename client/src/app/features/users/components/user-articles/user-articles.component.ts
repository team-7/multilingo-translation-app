import {
  Component,
  OnInit,
  Input,
  ViewChild,
  EventEmitter,
  Output
} from '@angular/core';
import { MatTableDataSource, MatPaginator, MatSort } from '@angular/material';
import { ShowUserDTO } from '../../models/show-users.dto';
import { UsersService } from '../../../../core/services/users.service';
import { UIComponentDTO } from '../../../../models/ui-component.dto';
import { NotificationService } from '../../../../core/services/notification.service';
import { ShowArticleVersionDTO } from '../../../articles/models/show-article-version.dto';

@Component({
  selector: 'app-user-articles',
  templateUrl: './user-articles.component.html',
  styleUrls: ['./user-articles.component.css']
})
export class UserArticlesComponent implements OnInit {
  public displayedColumns: string[] = ['title', 'originalLn', 'category'];
  public dataSource: MatTableDataSource<ShowArticleVersionDTO>;

  @Output()
  public goToArticleEvent = new EventEmitter<string>();

  @Input()
  public articles: ShowArticleVersionDTO[];
  public filteredArticles;

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  @Input()
  public isOwnerOrAdmin: boolean;

  @Input()
  public user: ShowUserDTO;

  @Input()
  public ui: UIComponentDTO;

  constructor(
    private readonly usersService: UsersService,
    private readonly notificationsService: NotificationService
  ) {}

  ngOnInit() {
    this.usersService.getAllArticleByUser(this.user.id).subscribe(
      (articles) => {
        this.articles = articles;
        this.filteredArticles = this.articles;
        const articlesSource = [...this.filteredArticles];
        articlesSource.map((val, index) => {
          // tslint:disable-next-line: no-string-literal
          val['tableId'] = index + 1;
        });
        this.dataSource = new MatTableDataSource(articlesSource);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      },
      (err) => {
        this.notificationsService.error(err.error.message);
      }
    );
  }

  public searchArticles(filterValue: string): void {
    this.filteredArticles = this.articles;
    this.filteredArticles = this.filteredArticles.filter(article => {
      if (
        article.title.toLowerCase().includes(filterValue.trim().toLowerCase())
      ) {
        return article;
      }
    });
    this.dataSource.data = this.filteredArticles;

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  public goToArticle(articleId: string): void {
    this.goToArticleEvent.emit(articleId);
  }
}
