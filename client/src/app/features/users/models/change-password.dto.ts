export class ChangePasswordDTO {
    public newPass: string;
    public oldPass: string;
    public repeatNewPass: string;
}
