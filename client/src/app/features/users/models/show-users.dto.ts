export class ShowUserDTO {
    public id: string;
    public username: string;
    public name: string;
    public surname: string;
    public createdOn: Date;
    public email: string;
    public powerPoints: number;
    public profileImg: string;
    public isBanned: boolean;
    public endOfBanDate: Date;
    public banReason: string;
    public roles: string[];
    public isDeleted: boolean;
    public prefLang: string;
    public editLanguages: string[];
}
