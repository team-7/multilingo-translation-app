export class UpdateBanStatusDTO {
    public banReason: string;
    public days: number;
}
