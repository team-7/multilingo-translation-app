export class LoginCredentialsDTO {
    public username: string;
    public password: string;
    public keepMeLoggedIn?: boolean;
}
