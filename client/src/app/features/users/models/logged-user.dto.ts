export class LoggedUserDTO {
    public id: string;
    public username: string;
    public profileImg: string;
    public roles: string[];
    public isBanned: boolean;
    public endOfBanDate: Date;
    public banReason: string;
    public powerPoints: number;
    public prefLang: string;
    public editLanguages?: string[];
}
