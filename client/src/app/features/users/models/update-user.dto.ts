export class UpdateUserDTO {
    public name?: string;
    public surname?: string;
    public createdOn?: Date;
    public email?: string;
    public profileImg?: string;
}
