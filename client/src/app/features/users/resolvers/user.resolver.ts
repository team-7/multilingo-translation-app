import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { NotificationService } from '../../../core/services/notification.service';
import { UsersService } from '../../../core/services/users.service';
import { ShowUserDTO } from '../models/show-users.dto';

@Injectable({
    providedIn: 'root'
})
export class UserResolverService implements Resolve<ShowUserDTO> {

  constructor(
    private readonly usersService: UsersService,
    private readonly router: Router,
    private readonly notificationService: NotificationService,
  ) { }

  resolve(
    route: ActivatedRouteSnapshot,
  ): Observable<ShowUserDTO> {
    const id = route.paramMap.get('id');

    return this.usersService.getUserById(id)
      .pipe(
        map(user => {
          if (user) {
            return user;
          } else {
            this.router.navigate(['/not-found']);
            this.notificationService.error(`User not found!`);
            return;
          }
        }),
      );
  }
}
