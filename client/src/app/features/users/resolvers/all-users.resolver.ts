import { Injectable } from '@angular/core';
import { Resolve, Router } from '@angular/router';
import { map } from 'rxjs/operators';
import { NotificationService } from '../../../core/services/notification.service';
import { UsersService } from '../../../core/services/users.service';
import { ShowUserDTO } from '../models/show-users.dto';

@Injectable({
    providedIn: 'root'
})
export class AllUsersResolver implements Resolve<ShowUserDTO[]> {

  constructor(
    private readonly router: Router,
    private readonly usersService: UsersService,
    private readonly notificationService: NotificationService,
  ) {
  }

  resolve() {
    return this.usersService.allUsers()
      .pipe(
        map(users => {
          if (users) {
            return users;
          } else {
            this.router.navigate(['/']);
            this.notificationService.error(`There was an unexpected error.`);
            return;
          }
        })
      );
  }
}
