import { CommentsService } from './../../../core/services/comment.service';
import { AllCommentsComponent } from './comments.component';
import { CommentInfoComponent } from './../comment-info/comment-info.component';
import { of, throwError } from 'rxjs';
import { ComponentFixture, async, TestBed } from '@angular/core/testing';
import { SharedModule } from '../../../shared/shared.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule } from '@angular/forms';
import { CreateCommentComponent } from '../create-comment/create-comment.component';
import { EditCommentComponent } from '../edit-comment/edit-comment.component';
import { AuthService } from '../../../core/services/auth.service';
import { NotificationService } from '../../../core/services/notification.service';
import { DialogService } from '../../../core/services/dialog.service';
import { LanguagesService } from '../../../core/services/languages.service';

describe('AllCommentsComponent', () => {

  let fixture: ComponentFixture<AllCommentsComponent>;
  let component: AllCommentsComponent;

  const commentsService = {
    getArticleComments() {
        return of([]);
    },
    editComment() {
        return of({});
    },
    detectCommentLang() {
        return of({});
    },
    createComment() {
        return of({});
    }
    };
  const authService = {
    get loggedUser$() {
        return of({});
    }
  };
  const dialogService = {
    open() { },
    close() { }
    };
  const notificationService = {
        success() {},
        error() {},
    };
  const langService = {
        getAllLanguages() {
            return of([]);
        }
    };

  beforeEach(async(() => {
    // clear all spies and mocks
    jest.clearAllMocks();

    TestBed.configureTestingModule({
      imports: [
        SharedModule,
        FormsModule,
        BrowserAnimationsModule,
      ],
      declarations: [
        AllCommentsComponent,
        CreateCommentComponent,
        CommentInfoComponent,
        EditCommentComponent,
    ],
      providers: [
        CommentsService,
        AuthService,
        NotificationService,
        DialogService,
        LanguagesService,
      ]
    })
    .overrideProvider(CommentsService, {useValue: commentsService})
    .overrideProvider(AuthService, {useValue: authService})
    .overrideProvider(NotificationService, {useValue: notificationService})
    .overrideProvider(DialogService, {useValue: dialogService})
    .overrideProvider(LanguagesService, {useValue: langService})
    .compileComponents();

  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AllCommentsComponent);
    component = fixture.componentInstance;
    component.articleId = 'qwert';
    component.language = 'en';
    // fixture.detectChanges();
  });

  it('should be defined', () => {
    expect(component).toBeDefined();
});

  describe(' ngOnInit should', () => {

    it('initialize correctly with the data passed from the authService', done => {
        // Arrange
        const loggedUser = { };

        const spy = jest.spyOn(authService, 'loggedUser$', 'get').mockImplementation(() => of(loggedUser));

        // Act
        component.ngOnInit();

        // Assert
        expect(component.loggedUser).toBe(loggedUser);

        done();
    });

    it('throw error with the data passed from the authService', done => {
        // Arrange
        const message = '';

        const spy = jest.spyOn(authService, 'loggedUser$', 'get').mockImplementation(() => throwError(message));
        const notification = jest.spyOn(notificationService, 'error').mockImplementation();

        // Act
        component.ngOnInit();

        // Assert
        expect(notification).toHaveBeenCalledTimes(1);

        done();
    });

    it('initialize correctly with the data passed from the langService', done => {
        // Arrange
        const languages = [{

        }];

        const spy = jest.spyOn(langService, 'getAllLanguages').mockImplementation(() => of(languages));

        // Act
        component.ngOnInit();

        // Assert
        expect(component.allLangs).toBe(languages);

        done();
    });

    it('throw error with the data passed from the langService', done => {
        // Arrange
        const message = '';
        const loggedUser = { };

        const spy = jest.spyOn(langService, 'getAllLanguages').mockImplementation(() => throwError(message));
        const notification = jest.spyOn(notificationService, 'error');
        const spy1 = jest.spyOn(authService, 'loggedUser$', 'get').mockImplementation(() => of(loggedUser));
        const spy2 = jest.spyOn(commentsService, 'getArticleComments').mockImplementation(() => of());
        // Act
        component.ngOnInit();

        // Assert
        expect(notification).toHaveBeenCalledTimes(1);

        done();
    });

    it('initialize correctly with the data passed from the commentsService', done => {
        // Arrange
        const comments = [{

        }];

        const spy = jest.spyOn(commentsService, 'getArticleComments').mockImplementation(() => of(comments));

        // Act
        component.ngOnInit();

        // Assert
        expect(component.comments).toStrictEqual(comments);

        done();
    });

    it('throw error with the data passed from the commentsService', done => {
        // Arrange
        const message = '';
        const languages = [{

        }];
        const loggedUser = { };

        const spy = jest.spyOn(commentsService, 'getArticleComments').mockImplementation(() => throwError(message));
        const notification = jest.spyOn(notificationService, 'error').mockImplementation();
        const spy1 = jest.spyOn(langService, 'getAllLanguages').mockImplementation(() => of(languages));
        const spy2 = jest.spyOn(authService, 'loggedUser$', 'get').mockImplementation(() => of(loggedUser));

        // Act
        component.ngOnInit();

        // Assert
        expect(notification).toHaveBeenCalledTimes(1);

        done();
    });

    });

  describe('editComment()', () => {
        it('should have called editComment() from commentService', () => {
            // Arrange
            const spy = jest.spyOn(commentsService, 'editComment').mockImplementation(() => of());
            const comment = {commentId: '1', newText: {content: 'wood'}};
            // Act
            component.editComment(comment);

            // Assert
            expect(spy).toHaveBeenCalledTimes(1);
        });
        it('commentService should have returned successful message', done => {
            // Arrange
            const comment = {commentId: '1', newText: {content: 'wood'}};
            const spy = jest.spyOn(commentsService, 'editComment').mockImplementation(() => of(5));
            const notification = jest.spyOn(notificationService, 'success');
            // Act
            component.editComment(comment);

            // Assert
            expect(notification).toHaveBeenCalledTimes(1);

            done();
        });
        it('commentService should have returned error message', done => {
            // Arrange
            const message = '';
            const spy = jest.spyOn(commentsService, 'editComment').mockImplementation(() => throwError(message));
            const notification = jest.spyOn(notificationService, 'error').mockImplementation();
            const comment = {commentId: '1', newText: {content: 'wood'}};
            // Act
            component.editComment(comment);

            // Assert
            expect(notification).toHaveBeenCalledTimes(1);

            done();
        });
    });


  describe('createComment()', () => {
        it('should have called detectCommentLang() from commentService', () => {
            // Arrange
            const spy = jest.spyOn(commentsService, 'detectCommentLang').mockImplementation(() => of());
            // Act
            component.createComment({ content: 'tests'});

            // Assert
            expect(spy).toHaveBeenCalledTimes(1);
        });
        it('commentService should have returned successful message', done => {
            // Arrange
            const comment = {content: 'wood'};
            const languages = [{

            }];
            const commentsarr = [{

            }];

            const spy = jest.spyOn(commentsService, 'getArticleComments').mockImplementation(() => of(commentsarr));
            const spy1 = jest.spyOn(langService, 'getAllLanguages').mockImplementation(() => of(languages));
            const spy2 = jest.spyOn(commentsService, 'detectCommentLang').mockImplementation(() => of(5));
            const spy3 = jest.spyOn(dialogService, 'open')
                .mockImplementation(() => ({
                    componentInstance: {
                        choosenLang: of('lang'),
                    }
                }));
            const spy4 = jest.spyOn(commentsService, 'createComment').mockImplementation(() => of(5));
            const notification = jest.spyOn(notificationService, 'success');
            // Act
            component.ngOnInit();
            component.createComment(comment);

            // Assert
            expect(notification).toHaveBeenCalledTimes(1);

            done();
        });
    });
});


