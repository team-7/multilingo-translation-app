import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-create-comment',
  templateUrl: './create-comment.component.html',
  styleUrls: ['./create-comment.component.css']
})
export class CreateCommentComponent implements OnInit {

  public createCommentForm: FormGroup;

  @Output() newCommentEvent = new EventEmitter<{content: string}>();

  constructor(
    private readonly fb: FormBuilder,
  ) { }

  ngOnInit() {
    this.createCommentForm = this.fb.group({
      content: [``, Validators.compose([Validators.required, Validators.minLength(1), Validators.maxLength(500)])],
    });
  }

  createComment() {
    this.newCommentEvent.emit(this.createCommentForm.value);
    this.createCommentForm.reset();
  }

}
