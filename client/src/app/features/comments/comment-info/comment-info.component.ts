import { Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { ShowUserDTO } from '../../users/models/show-users.dto';
import { CommentDTO } from '../models/comment.dto';

@Component({
  selector: 'app-comment-info',
  templateUrl: './comment-info.component.html',
  styleUrls: ['./comment-info.component.css']
})
export class CommentInfoComponent implements OnInit, OnDestroy {

  @Input() comment: CommentDTO;
  @Input() user: ShowUserDTO;
  @Input() commentLang: string;
  @Output() newTextEvent = new EventEmitter<{commentId: string, newText: {content: string}}>();
  public canEdit: boolean;
  public isEditMenuOpen = false;

  constructor() { }

  ngOnInit() {
    this.canEdit = this.user && this.user.roles.length > 1 ? true : false;
  }

  ngOnDestroy(): void {
  }

  toggleEditMenu() {
    this.isEditMenuOpen = !this.isEditMenuOpen;
  }

  editComment(newText: {content: string}) {
    const editedComment = {
      commentId: this.comment.id,
      newText
    };
    this.comment.content = newText.content;
    this.newTextEvent.emit(editedComment);
  }

}
