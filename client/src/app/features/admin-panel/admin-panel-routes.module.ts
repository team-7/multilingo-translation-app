import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { adminRoutes } from './admin-panel.routes';

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(adminRoutes)
    ],
    exports: [RouterModule]
})
export class AdminPanelRoutingModule {}
