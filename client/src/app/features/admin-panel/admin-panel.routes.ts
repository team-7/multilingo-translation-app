import { Routes } from '@angular/router';
import { AdminGuard } from '../../core/guards/admin.guard';
import { AdminPanelComponent } from './components/admin-panel/admin-panel.component';
import { AllUsersResolver } from './resolvers/all-users.resolver';
import { AllLanguagesResolver } from './resolvers/all-languages.resolver';
import { SuppLanguagesResolver } from './resolvers/supp-languages.resolver';


export const adminRoutes: Routes = [
    {
        path: '',
        component: AdminPanelComponent,
        resolve: {
            users: AllUsersResolver,
            allLanguages: AllLanguagesResolver,
            suppLanguages: SuppLanguagesResolver,
        },
        canActivate: [AdminGuard],
    },
];
