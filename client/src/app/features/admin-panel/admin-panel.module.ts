import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { SharedModule } from '../../shared/shared.module';
import { AdminPanelRoutingModule } from './admin-panel-routes.module';
import { AdminArticlesComponent } from './components/admin-articles/admin-articles.component';
import { AdminCommentsComponent } from './components/admin-comments/admin-comments.component';
import { AdminItemsComponent } from './components/admin-items/admin-items.component';
import { AdminPanelComponent } from './components/admin-panel/admin-panel.component';
import { AdminSettingsComponent } from './components/admin-settings/admin-settings.component';
import { AdminUsersDialogComponent } from './components/admin-users/admin-users-dialog/admin-users-dialog.component';
import { AdminUsersComponent } from './components/admin-users/admin-users.component';
import { ChangeLanguagesComponent } from './components/admin-settings/change-languages/change-languages.component';
import { AllLanguagesResolver } from './resolvers/all-languages.resolver';
import { SuppLanguagesResolver } from './resolvers/supp-languages.resolver';
import { AllUsersResolver } from './resolvers/all-users.resolver';
import { CreateCategoryComponent } from './components/admin-settings/categories/create-category/create-category.component';
import { CategoriesService } from '../../core/services/categories.service';
import { ArticlesService } from '../../core/services/articles.service';
import { CommentsService } from '../../core/services/comment.service';

@NgModule({
  declarations: [
    AdminPanelComponent,
    AdminArticlesComponent,
    AdminItemsComponent,
    AdminItemsComponent,
    AdminUsersComponent,
    AdminSettingsComponent,
    AdminUsersDialogComponent,
    AdminCommentsComponent,
    ChangeLanguagesComponent,
    CreateCategoryComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    AdminPanelRoutingModule
  ],
  providers: [
    AllLanguagesResolver,
    SuppLanguagesResolver,
    AllUsersResolver,
    CategoriesService,
    ArticlesService,
    CommentsService
  ],
  entryComponents: [
    AdminUsersDialogComponent,
  ],

})
export class AdminPanelModule { }
