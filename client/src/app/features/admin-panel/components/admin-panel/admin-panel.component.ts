import { Component, OnInit } from '@angular/core';
import { ShowUserDTO } from '../../../users/models/show-users.dto';
import { ActivatedRoute, Router } from '@angular/router';
import { LanguageDTO } from '../../../languages/models/language.dto';
import { AuthService } from '../../../../core/services/auth.service';
import { LoggedUserDTO } from '../../../users/models/logged-user.dto';
import { ArticlesService } from '../../../../core/services/articles.service';
import { ShowArticleVersionDTO } from '../../../articles/models/show-article-version.dto';
import { NotificationService } from '../../../../core/services/notification.service';
import { UIService } from '../../../../core/services/ui.service';
import { UIComponentDTO } from '../../../../models/ui-component.dto';

@Component({
  selector: 'app-admin-panel',
  templateUrl: './admin-panel.component.html',
  styleUrls: ['./admin-panel.component.css']
})
export class AdminPanelComponent implements OnInit {

  public users: ShowUserDTO[];
  public articles: ShowArticleVersionDTO[];
  public allLanguages: LanguageDTO[];
  public suppLanguages: LanguageDTO[];
  public loggedUser: LoggedUserDTO;
  public ui: UIComponentDTO;

  constructor(
    private readonly route: ActivatedRoute,
    private readonly router: Router,
    private readonly authService: AuthService,
    private readonly articlesService: ArticlesService,
    private readonly notificationsService: NotificationService,
    private readonly uiService: UIService
  ) {
    const observable = this.route.data.subscribe(
      (data) => {
        this.users = [ ...data.users ];
        this.allLanguages = [ ...data.allLanguages ];
        this.suppLanguages = [ ...data.suppLanguages ];
      },
      (err) => {
        this.notificationsService.error(err.error.message);
      }
    );
    observable.unsubscribe();
  }

  ngOnInit() {
    this.uiService.UI$.subscribe(
      (ui: UIComponentDTO) => {
        this.ui = ui;
      },
      err => {
        this.notificationsService.error(err.error.message);
        this.router.navigate(['home']);
      }
    );
  }
}
