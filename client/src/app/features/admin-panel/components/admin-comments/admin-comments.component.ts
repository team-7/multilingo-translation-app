import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { MatTableDataSource, MatPaginator, MatSort } from '@angular/material';
import { ShowUserDTO } from '../../../users/models/show-users.dto';
import { ArticlesService } from '../../../../core/services/articles.service';
import { LoggedUserDTO } from '../../../users/models/logged-user.dto';
import { CommentsService } from '../../../../core/services/comment.service';
import { NotificationService } from '../../../../core/services/notification.service';
import { AuthService } from '../../../../core/services/auth.service';
import { UIComponentDTO } from '../../../../models/ui-component.dto';

@Component({
  selector: 'app-admin-comments',
  templateUrl: './admin-comments.component.html',
  styleUrls: ['./admin-comments.component.css']
})
export class AdminCommentsComponent implements OnInit {



  displayedColumns: string[] = ['content', 'originalLn', 'isDeleted'];
  dataSource: MatTableDataSource<ShowUserDTO>;

  @Input()
  public comments;
  public filteredComments;
  public loggedUser: LoggedUserDTO;

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;
  @Input()
  public ui: UIComponentDTO;

  constructor(
    private readonly authService: AuthService,
    private readonly commentsService: CommentsService,
    private readonly notificationsService: NotificationService,
    private readonly articlesService: ArticlesService,
  ) {}

  ngOnInit() {
    this.authService.loggedUser$.subscribe(loggedUser => {
      this.loggedUser = loggedUser;
      this.articlesService.getAllArticles(this.loggedUser.prefLang).subscribe(
        articles => {
          this.comments = articles.map((article) => {
            let articleComments = [];
            this.commentsService.getArticleComments(article.article.id, this.loggedUser.prefLang).subscribe(
              (comments) => {
                articleComments = [...articleComments, ...comments];
              },
              () => {

              }
            );
            return articleComments;
          });
          this.filteredComments = this.comments;
          const commentsSource = [...this.filteredComments];
          commentsSource.map((val, index) => {
            // tslint:disable-next-line: no-string-literal
            val['tableId'] = index + 1;
          });
          this.dataSource = new MatTableDataSource(commentsSource);
          this.dataSource.paginator = this.paginator;
          this.dataSource.sort = this.sort;
        },
        err => {
          this.notificationsService.error(err.error.message);
        }
      );
    });
  }

  public searchComments(filterValue: string) {
    this.filteredComments = this.comments;
    this.filteredComments = this.filteredComments.filter((comment) => {
      if (comment.content.toLowerCase().includes(filterValue.trim().toLowerCase())) {
        return comment;
      }
    });
    this.dataSource.data = this.filteredComments;

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

}
