import { Component, OnInit, Input } from '@angular/core';
import { LanguageDTO } from '../../../languages/models/language.dto';
import { UIComponentDTO } from '../../../../models/ui-component.dto';

@Component({
  selector: 'app-admin-settings',
  templateUrl: './admin-settings.component.html',
  styleUrls: ['./admin-settings.component.css']
})

export class AdminSettingsComponent implements OnInit {

  @Input()
  public allLanguages: LanguageDTO[];
  @Input()
  public suppLanguages: LanguageDTO[];
  @Input()
  public ui: UIComponentDTO;

  constructor() { }

  ngOnInit() {
  }

}
