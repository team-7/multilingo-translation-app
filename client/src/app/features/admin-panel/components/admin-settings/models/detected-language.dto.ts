export class DetectedLanguageDTO {
  public confidence: string;
  public language: string;
  public input: string;
}
