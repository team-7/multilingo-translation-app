import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { MatTableDataSource, MatPaginator, MatSort } from '@angular/material';
import { ShowUserDTO } from '../../../users/models/show-users.dto';
import { UIService } from '../../../../core/services/ui.service';
import { AuthService } from '../../../../core/services/auth.service';
import { LoggedGuard } from '../../../../core/guards/logged.guard';
import { LoggedUserDTO } from '../../../users/models/logged-user.dto';
import { NotificationService } from '../../../../core/services/notification.service';
import { UIComponentDTO } from '../../../../models/ui-component.dto';

@Component({
  selector: 'app-admin-items',
  templateUrl: './admin-items.component.html',
  styleUrls: ['./admin-items.component.css']
})
export class AdminItemsComponent implements OnInit {

  displayedColumns: string[] = ['text', 'resourceName'];
  dataSource: MatTableDataSource<ShowUserDTO>;


  public allUi = [];
  public filteredUI;
  public loggedUser: LoggedUserDTO;

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;
  @Input()
  public ui: UIComponentDTO;

  constructor(
    private readonly authService: AuthService,
    private readonly uiService: UIService,
    private readonly notificationsService: NotificationService
  ) {
  }

  ngOnInit() {
    this.authService.loggedUser$.subscribe(loggedUser => {
      this.loggedUser = loggedUser;
      this.uiService.getAllUIInLang(this.loggedUser.prefLang).subscribe(
        uiData => {
          // tslint:disable-next-line: forin
          for (const a in uiData) {
            this.allUi.push({
              resourceName: a,
              text: uiData[a],
            });
          }
          this.filteredUI = this.allUi;
          const uiSource = [...this.filteredUI];
          uiSource.map((val, index) => {
            // tslint:disable-next-line: no-string-literal
            val['tableId'] = index + 1;
          });
          this.dataSource = new MatTableDataSource(uiSource);
          this.dataSource.paginator = this.paginator;
          this.dataSource.sort = this.sort;
          this.dataSource.paginator = this.paginator;
          this.dataSource.sort = this.sort;
        },
        err => {
          this.notificationsService.error(err.error.message);
        }
      );
    });
  }

  public searchItems(filterValue: string) {
    this.filteredUI = this.ui;
    this.filteredUI = this.filteredUI.filter((item) => {
      if (item.content.toLowerCase().includes(filterValue.trim().toLowerCase())) {
        return item;
      }
    });
    this.dataSource.data = this.filteredUI;

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  public deleteItem(_itemId: string) {
    // Delete
  }

}
