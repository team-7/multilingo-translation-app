import { Injectable } from '@angular/core';
import { Resolve, Router } from '@angular/router';
import { map } from 'rxjs/operators';
import { LanguagesService } from '../../../core/services/languages.service';
import { NotificationService } from '../../../core/services/notification.service';
import { LanguageDTO } from '../../languages/models/language.dto';

@Injectable({
    providedIn: 'root'
})
export class AllLanguagesResolver implements Resolve<LanguageDTO[]> {

  constructor(
    private readonly router: Router,
    private readonly notificationService: NotificationService,
    private readonly languagesService: LanguagesService
  ) {
  }

  resolve() {
    return this.languagesService.getAllLanguages()
      .pipe(
        map(languages => {
          if (languages) {
            return languages;
          } else {
            this.router.navigate(['/']);
            this.notificationService.error(`There was an unexpected error.`);
            return;
          }
        })
      );
  }
}
