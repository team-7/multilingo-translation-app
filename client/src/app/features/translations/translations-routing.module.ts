import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { AllTranslationsComponent } from './all-translations/all-translations.component';
import { AllTranslationsResolver } from './resolvers/all-translations.resolver';

const routes = [
  {
    path: '',
    component: AllTranslationsComponent,
    resolve: { translations: AllTranslationsResolver }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TranslationsRoutingModule {}
