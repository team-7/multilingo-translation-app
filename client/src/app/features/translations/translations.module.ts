import { NgModule } from '@angular/core';
import { SharedModule } from '../../shared/shared.module';
import { AllTranslationsComponent } from './all-translations/all-translations.component';
import { AllTranslationsResolver } from './resolvers/all-translations.resolver';
import { TranslationsRoutingModule } from './translations-routing.module';

@NgModule({
  declarations: [AllTranslationsComponent],
  imports: [SharedModule, TranslationsRoutingModule],
  providers: [AllTranslationsResolver]
})
export class TranslationsModule {}
