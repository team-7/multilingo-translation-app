import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { DialogService } from '../../../core/services/dialog.service';
import { TranslationsService } from '../../../core/services/translation.service';
import { UIService } from '../../../core/services/ui.service';
import { UIComponentDTO } from '../../../models/ui-component.dto';
import { EditTranslationDialogComponent } from '../edit-translation-dialog/edit-translation-dialog.component';
import { TranslationDTO } from '../models/translation.dto';

@Component({
  selector: 'app-all-translations',
  templateUrl: './all-translations.component.html',
  styleUrls: ['./all-translations.component.css']
})
export class AllTranslationsComponent implements OnInit {
  public ui: UIComponentDTO;
  public translations: TranslationDTO[];

  displayedColumns: string[] = [
    'tableId',
    'originalText',
    'translatedText',
    'language',
    'targetLanguage',
    'type',
    'rating',
    'translatedBy'
  ];
  dataSource: MatTableDataSource<TranslationDTO>;

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  constructor(
    private readonly route: ActivatedRoute,
    private dialogService: DialogService,
    private readonly translService: TranslationsService,
    private readonly router: Router,
    private readonly uiService: UIService
  ) {}

  ngOnInit() {
    this.route.data.subscribe(({translations}) => {
      this.translations = [ ...translations.articles, ...translations.uiTransl];

    });

    this.uiService.UI$.subscribe((ui: UIComponentDTO) => {
      this.ui = ui;
    });

    this.dataSource = new MatTableDataSource(
      this.translations.map((e, index) => {
        // tslint:disable-next-line: no-string-literal
        e['tableId'] = index;
        return e;
      })
    );

    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  public triggerTranslationUpdate(translation: TranslationDTO) {
    const dialogRef = this.dialogService.open(
      EditTranslationDialogComponent,
      translation
    );
    const updateEvent = dialogRef.componentInstance.updateTranslation.subscribe(
      ({ translatedText }) => {
        this.translService
          .updateTransl(translation.id, translatedText)
          .subscribe((data: TranslationDTO) => {
            this.translations.map((e: TranslationDTO) => {
              if (e.id === data.id) {
                e.translatedText = data.translatedText;
                e.translatedBy = data.translatedBy;
                return e;
              }
              return e;
            });
          });
        dialogRef.close();
      }
    );
  }

  public goToTranslatorProfile(id: string) {
    if (!id) {
      return;
    }
    this.router.navigateByUrl(`users/${id}`);
  }
}
