import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../../../core/services/auth.service';
import { LanguagesService } from '../../../core/services/languages.service';
import { NotificationService } from '../../../core/services/notification.service';
import { UIService } from '../../../core/services/ui.service';
import { LanguageDTO } from '../../../features/languages/models/language.dto';
import { LoggedUserDTO } from '../../../features/users/models/logged-user.dto';
import { UIComponentDTO } from '../../../models/ui-component.dto';

@Component({
  selector: 'app-navigation',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.css']
})
export class NavigationComponent implements OnInit {
  @Input() ui: UIComponentDTO;

  constructor(
    private readonly router: Router,
    private readonly authService: AuthService,
    private readonly languagesService: LanguagesService,
    private readonly uiService: UIService,
    private readonly notificationService: NotificationService
  ) {}

  public suppLangs: LanguageDTO[] = [];
  public loggedUser: LoggedUserDTO;
  public detectedLanguage: string;
  public selectedLang: string;

  ngOnInit() {
    this.authService.loggedUser$.subscribe(loggedUser => {
      this.loggedUser = loggedUser;
    });

    const suppLangSub = this.languagesService
      .getSuppLanguages()
      .subscribe((suppLangs: LanguageDTO[]) => {
        this.suppLangs = suppLangs;
      });

    this.languagesService.displayLanguage$.subscribe((lang: string) => {
      this.selectedLang = lang;
    });
  }

  public onChange() {
    // this.languagesService.changeLanguage(this.selectedLang);
    this.uiService.fireUISubjectInLang(this.selectedLang).subscribe(_ => {
      this.languagesService.changeLanguage(this.selectedLang);
    });
  }

  public toUsers() {
    this.router.navigate(['/users']);
  }

  public toUserProfile() {
    this.router.navigate([`/users/`, this.loggedUser.id]);
  }

  public toAdminPanel() {
    this.router.navigate(['/admin-panel']);
  }

  public toSignIn() {
    this.router.navigate(['/landing']);
  }

  public toLogout() {
    try {
      this.authService.logout();
      this.notificationService.success('Logout successful!');
      // const detectedLang = navigator.language.split('-')[0];

      const languages = navigator.languages;
      let done = false;

      for (let curr of languages) {
        if (curr.match('-')) {
          curr = curr.split('-')[0];
        }
        const foundLang = this.suppLangs.find(e => e.index === curr);

        if (foundLang) {
          this.languagesService.changeLanguage(foundLang.index);
          this.uiService
            .fireUISubjectInLang(foundLang.index)
            .subscribe(_ => {});
          done = true;
          break;
        }
      }

      if (!done) {
        this.uiService.fireUISubjectInLang('en').subscribe(_ => {
          this.languagesService.changeLanguage('en');
        });
      }

      this.router.navigate(['/home']);
    } catch (error) {
      this.router.navigate(['/']);
    }
  }

  public toArticles() {
    this.router.navigate(['/articles']);
  }

  public toCreateArticles() {
    this.router.navigate(['/articles/create']);
  }

  public toTranslations() {
    this.router.navigate(['/translations']);
  }

  public toCreateTranslation() {
    this.router.navigate(['/translation/create']);
  }

  public toCategories() {
    this.router.navigate(['/articles/categories/news']);
  }
}
