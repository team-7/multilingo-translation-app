import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CategoriesService } from '../../../core/services/categories.service';
import { CategoryDTO } from '../../../features/admin-panel/components/admin-settings/categories/models/category.dto';

@Component({
  selector: 'app-categories-navigation',
  templateUrl: './categories-navigation.component.html',
  styleUrls: ['./categories-navigation.component.css']
})
export class CategoriesNavigationComponent implements OnInit {
  public language: string;
  @Input() public categories: CategoryDTO[];

  constructor(
    private readonly router: Router,
    private readonly categoriesService: CategoriesService
  ) {}

  ngOnInit() {
  }

  public selectCategory(category: CategoryDTO) {
    this.categoriesService
      .getCategoryById(category.id)
      .subscribe((originalCategory: CategoryDTO) => {
        this.router.navigateByUrl(
          `articles/categories/${originalCategory.name}`
        );
      });
  }
}
