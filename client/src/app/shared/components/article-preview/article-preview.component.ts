import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';
import { ShowArticleVersionDTO } from '../../../features/articles/models/show-article-version.dto';
import { UIComponentDTO } from '../../../models/ui-component.dto';

@Component({
  selector: 'app-article-preview',
  templateUrl: './article-preview.component.html',
  styleUrls: ['./article-preview.component.css']
})
export class ArticlePreviewComponent implements OnInit {
  @Input() ui: UIComponentDTO;
  @Input() article: ShowArticleVersionDTO;

  @Output() goToArticleId = new EventEmitter<string>();

  constructor(private readonly router: Router) {}

  ngOnInit() {}

  public seeArticle(id: string) {
    this.goToArticleId.emit(id);
  }

  public viewUser(id: string) {
    this.router.navigateByUrl(`/users/${id}`);
  }
}
