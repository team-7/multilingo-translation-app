import { Component, EventEmitter, Inject, OnInit, Output } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { LanguageDTO } from '../../../features/languages/models/language.dto';
import { UIComponentDTO } from '../../../models/ui-component.dto';

@Component({
  selector: 'app-language-confirm-dialog',
  templateUrl: './language-confirm-dialog.component.html',
  styleUrls: ['./language-confirm-dialog.component.css']
})
export class LanguageConfirmDialogComponent implements OnInit {
  public ui: UIComponentDTO;
  public selectedLang: string;
  @Output() choosenLang: EventEmitter<LanguageDTO> = new EventEmitter();
  public listOfLanguages: string[];
  constructor(
    public dialogRef: MatDialogRef<LanguageConfirmDialogComponent>,
    @Inject(MAT_DIALOG_DATA)
    public data: {
      detectedLang: LanguageDTO;
      suppLangs: LanguageDTO[];
      ui: UIComponentDTO;
    }
  ) {
    this.listOfLanguages = data.suppLangs.map(e => e.name);
    this.ui = this.data.ui;
  }

  ngOnInit() {}

  onYesClick() {
    this.choosenLang.emit(this.data.detectedLang);
  }

  onNotInTheListClick() {
    this.dialogRef.close();
  }

  onReadyClick() {
    const selected: LanguageDTO = this.data.suppLangs.find(
      e => e.name === this.selectedLang
    );

    this.choosenLang.emit(selected);
  }
}
