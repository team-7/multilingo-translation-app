import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LanguagesService } from '../../../core/services/languages.service';
import { UIService } from '../../../core/services/ui.service';
import { UIComponentDTO } from '../../../models/ui-component.dto';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  public ui: UIComponentDTO;

  constructor(
    private readonly router: Router,
    private readonly uiService: UIService,
    private readonly langService: LanguagesService,

  ) { }

  ngOnInit() {
    this.langService.displayLanguage$.subscribe(
      (lang: string) => {
        this.uiService.UI$.subscribe(
          (ui) => {
            this.ui = ui;

          }
        );
      }
    );
  }

  public redirectToSlash() {
    this.router.navigate(['']);
  }
}
