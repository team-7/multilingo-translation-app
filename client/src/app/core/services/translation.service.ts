import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { TranslationDTO } from '../../features/translations/models/translation.dto';

@Injectable()
export class TranslationsService {

  constructor(
    private readonly http: HttpClient,
  ) {}

  public getAllTranslations(type: string) {
    return this.http.get<TranslationDTO[]>(`http://localhost:3000/translations?type=${type}`);
  }

  public updateTransl(translId: string, text: string) {
    return this.http.put<TranslationDTO>(`http://localhost:3000/translations/${translId}`, { text });
  }

}
