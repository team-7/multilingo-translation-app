import { Injectable } from '@angular/core';
import { JwtHelperService } from '@auth0/angular-jwt';
import { LanguageDTO } from '../../features/languages/models/language.dto';
import { ShowUserDTO } from '../../features/users/models/show-users.dto';
import { LanguagesService } from './languages.service';
import { UIService } from './ui.service';

@Injectable({
  providedIn: 'root'
})
export class AppInitService {

  constructor(
    private readonly langService: LanguagesService,
    private readonly uiService: UIService,
  ) {}

  public async Init() {
    return new Promise<void>((resolve, reject) => {
      console.log('AppInitService.init() called');
      const localSToken = localStorage.getItem('token');
      const token = localSToken ? localSToken : sessionStorage.getItem('token');
      let user: ShowUserDTO = null;
      if (token) {
        user = (new JwtHelperService()).decodeToken(token);
      }
      if (!user) {

      this.langService.getSuppLanguages().subscribe(
            (suppLangs: LanguageDTO[]) => {
              const languages = navigator.languages;
              let done = false;

              for (let curr of languages) {

                if (curr.match('-')) {
                  curr = curr.split('-')[0];
                }
                const foundLang = suppLangs.find(e => e.index === curr);

                if (foundLang) {
                  this.uiService.fireUISubjectInLang(foundLang.index).subscribe(
                  (_) => {
                    this.langService.changeLanguage(foundLang.index);
                    resolve();
                  }
                  );
                  done = true;
                  break;
                }
              }

              if (!done) {

                this.uiService.fireUISubjectInLang('en').subscribe(
                  (_) => {
                    this.langService.changeLanguage('en');
                    resolve();
                  }
                );
              }

            }
          );
        } else {
          this.uiService.fireUISubjectInLang(user.prefLang).subscribe(
            (_) => {
              this.langService.changeLanguage(user.prefLang);
              resolve();
            }
          );
        }



  });
  }
}
