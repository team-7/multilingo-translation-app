import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { JwtHelperService } from '@auth0/angular-jwt';
import { BehaviorSubject, Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { LoggedUserDTO } from '../../features/users/models/logged-user.dto';
import { LoginCredentialsDTO } from '../../features/users/models/login-credential.dto';
import { RegisterUserDTO } from '../../features/users/models/register-user.dto';
import { TokenDTO } from '../../features/users/models/token.dto';
import { StorageService } from './storage.service';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  private readonly isLoggedSubject$: BehaviorSubject<boolean> = new BehaviorSubject(this.isUserLoggedIn());
  private readonly loggedUserSubject$: BehaviorSubject<LoggedUserDTO> = new BehaviorSubject(this.getUserDataIfAuthenticated());

  constructor(
    private readonly http: HttpClient,
    private readonly router: Router,
    private readonly storageService: StorageService,
    private readonly jwtService: JwtHelperService
  ) {}

  public login(credentials: LoginCredentialsDTO) {
    const logInUser: LoginCredentialsDTO = {
      username: credentials.username,
      password: credentials.password
    };
    return this.http
      .post<TokenDTO>(`http://localhost:3000/session`, logInUser)
      .pipe(
        tap(data => {
          try {
            const loggedUser: LoggedUserDTO = this.jwtService.decodeToken(data.token);
            const storageType: string = credentials.keepMeLoggedIn
              ? 'local'
              : 'session';

            this.storageService.setItem('token', data.token, storageType);

            this.isLoggedSubject$.next(true);
            this.loggedUserSubject$.next(loggedUser);
          } catch (error) {
            // proceed
          }
        })
      );
  }

  public logout() {
    return this.http.delete('http://localhost:3000/session').subscribe(
      (_) => {
        this.storageService.clear();

        this.isLoggedSubject$.next(false);
        this.loggedUserSubject$.next(null);
      },
      error => {
        this.router.navigate(['/']);
      }
    );
  }

  public register(user: RegisterUserDTO) {
    return this.http.post(`http://localhost:3000/users`, user);
  }

  //   public changePassword(passwords: ChangePasswordDTO) {
  //     return this.http.patch('http://localhost:3000/api/users/password', passwords);
  //   }

  private isUserLoggedIn() {
    const token: string = this.storageService.getItem('token');
    return !!token;
  }

  public get isLogged$(): Observable<boolean> {
    return this.isLoggedSubject$.asObservable();
  }

  public get loggedUser$(): Observable<LoggedUserDTO> {
    return this.loggedUserSubject$.asObservable();
  }

  public getDecodedAccessToken(token: string) {
    try {
      return this.jwtService.decodeToken(token);
    } catch (error) {
      // silent error
    }
  }

  public getUserDataIfAuthenticated(): LoggedUserDTO {
    const token: string = this.storageService.getItem('token');

    if (token && this.jwtService.isTokenExpired(token)) {
      this.storageService.delete('token');
      return null;
    }

    return token ? this.jwtService.decodeToken(token) : null;
  }
}
