import { UsersService } from './users.service';
import { AuthService } from './auth.service';
import { TestBed, async } from '@angular/core/testing';
import { of } from 'rxjs';
import { HttpClient } from '@angular/common/http';


describe('Users Service', () => {

    const http = {
        get() {
            return of();
        },
        post() {
            return of();
        },
        put() {
            return of();
        },
        delete() {
            return of();
        }
    };

    let getService: () => UsersService;

    beforeEach(async (() => {

        jest.clearAllMocks();

        TestBed.configureTestingModule({
            imports: [],
            declarations: [],
            providers: [
                HttpClient,
                UsersService
            ]
        }).overrideProvider(HttpClient, { useValue: http});

        getService = () => TestBed.get(UsersService);

    }));

    it('should be defined', () => {
        // Arrange & Act & Assert
        expect(getService).toBeDefined();
    });

    describe('allUsers() should', () => {
        it('call http.get() once', () => {
            // Arrange
            const users = [
                {
                    id: '1',
                    username: 'admin'
                },
                {
                   id: '2',
                   username: 'user'
               },
            ];
            const spy = jest.spyOn(http, 'get').mockReturnValue(of(users));
            // Act
            getService().allUsers();
            // Assert
            expect(spy).toHaveBeenCalled();
            expect(spy).toHaveBeenCalledTimes(1);

        });

        describe('getAllArticleByUser() should', () => {
            it('call http.get() once', () => {
                // Arrange
                const articles = [
                    {
                        id: '1',
                        title: 'Some content'
                    },
                    {
                        id: '2',
                        title: 'Other content'
                    }
                ];

                const userId = '10';

                const spy = jest.spyOn(http, 'get').mockReturnValue(of(articles));
                // Act
                getService().getAllArticleByUser(userId);
                // Assert
                expect(spy).toHaveBeenCalled();
                expect(spy).toHaveBeenCalledTimes(1);

            });
        });

        describe('getUserById() should', () => {
            it('call http.get() once', () => {
                // Arrange
                const user = {
                    id: '1',
                    username: 'admin'
                };

                const spy = jest.spyOn(http, 'get').mockReturnValue(of(user));
                const userId = '1';
                // Act
                getService().getUserById(userId);
                // Assert
                expect(spy).toHaveBeenCalled();
                expect(spy).toHaveBeenCalledTimes(1);

            });
        });

        describe('updateUser() should', () => {
            it('call http.put() once', () => {
                // Arrange
                const updateUser = {
                    id: '1',
                    username: 'admin',
                    name: 'John'
                };
                const spy = jest.spyOn(http, 'put').mockReturnValue(of(updateUser));
                const userId = '1';
                // Act
                getService().updateUser(userId, updateUser);
                // Assert
                expect(spy).toHaveBeenCalled();
                expect(spy).toHaveBeenCalledTimes(1);

            });
        });

        describe('deleteUser() should', () => {
            it('call http.delete() once', () => {
                // Arrange
                const responseMessage = {
                    message: 'Delete successful',
                    code: '200'
                };
                const spy = jest.spyOn(http, 'delete').mockReturnValue(of(responseMessage));
                const userId = '1';
                // Act
                getService().deleteUser(userId);
                // Assert
                expect(spy).toHaveBeenCalled();
                expect(spy).toHaveBeenCalledTimes(1);

            });
        });

        describe('changePassword() should', () => {
            it('call http.put() once', () => {
                // Arrange
                const newPassword = {
                    oldPass: '12345678',
                    newPass: '01234567',
                    repeatNewPass: '01234567'
                };
                const responseMessage = {
                    message: 'Change password successful'
                };
                const spy = jest.spyOn(http, 'put').mockReturnValue(of(responseMessage));
                const userId = '1';
                // Act
                getService().changePassword(userId, newPassword);
                // Assert
                expect(spy).toHaveBeenCalled();
                expect(spy).toHaveBeenCalledTimes(1);
            });
        });

        describe('changePrefLang() should', () => {
            it('call http.put() once', () => {
                // Arrange
                const prefLang = {
                    prefLang: 'en',
                };
                const responseMessage = {
                    message: 'Preferred language successful'
                };
                const spy = jest.spyOn(http, 'put').mockReturnValue(of(responseMessage));
                const userId = '1';
                // Act
                getService().changePrefLang(userId, prefLang);
                // Assert
                expect(spy).toHaveBeenCalled();
                expect(spy).toHaveBeenCalledTimes(1);
            });
        });

        describe('updateEditLanguages() should', () => {
            it('call http.put() once', () => {
                // Arrange
                const editLangs = {
                    editLanguages: ['en', 'sp'],
                };
                const user = {
                    id: '1',
                    username: 'admin',
                    editLanguages: ['en', 'sp']
                };
                const spy = jest.spyOn(http, 'put').mockReturnValue(of(user));
                const userId = '1';
                // Act
                getService().updateEditLanguages(userId, editLangs);
                // Assert
                expect(spy).toHaveBeenCalled();
                expect(spy).toHaveBeenCalledTimes(1);
            });
        });

        describe('changeRoles() should', () => {
            it('call http.put() once', () => {
                // Arrange
                const roles = {
                    roles: ['Admin', 'Editor', 'Contributor'],
                };
                const user = {
                    id: '1',
                    username: 'admin',
                    roles: ['Admin', 'Editor', 'Contributor'],
                };
                const spy = jest.spyOn(http, 'put').mockReturnValue(of(user));
                const userId = '1';
                // Act
                getService().changeRoles(userId, roles);
                // Assert
                expect(spy).toHaveBeenCalled();
                expect(spy).toHaveBeenCalledTimes(1);
            });
        });

        describe('banUser() should', () => {
            it('call http.post() once', () => {
                // Arrange
                const banStatus = {
                    banReason: 'Bad attitude!',
                    days: 5
                };
                const user = {
                    id: '1',
                    username: 'admin',
                    isBanned: true,
                    banReason: 'Bad attitude!',
                    days: 5
                };
                const spy = jest.spyOn(http, 'post').mockReturnValue(of(user));
                const userId = '1';
                // Act
                getService().banUser(userId, banStatus);
                // Assert
                expect(spy).toHaveBeenCalled();
                expect(spy).toHaveBeenCalledTimes(1);
            });
        });

        describe('unBanUser() should', () => {
            it('call http.delete() once', () => {
                // Arrange
                const user = {
                    id: '1',
                    username: 'admin',
                    isBanned: false,
                };
                const spy = jest.spyOn(http, 'delete').mockReturnValue(of(user));
                const userId = '1';
                // Act
                getService().unBanUser(userId);
                // Assert
                expect(spy).toHaveBeenCalled();
                expect(spy).toHaveBeenCalledTimes(1);
            });
        });

        describe('updateImage() should', () => {
            it('call http.put() once', () => {
                // Arrange
                const updatedUser = {
                    profileImg: 'https://imgur.com/myimage.png'
                };
                const spy = jest.spyOn(http, 'put').mockReturnValue(of(updatedUser));
                const userId = '1';
                // Act
                getService().updateImage(userId, updatedUser);
                // Assert
                expect(spy).toHaveBeenCalled();
                expect(spy).toHaveBeenCalledTimes(1);
            });
        });
    });
});
