import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { LanguageDTO } from '../../features/languages/models/language.dto';

@Injectable({
  providedIn: 'root',
})
export class LanguagesService {

  private readonly appLanguage$: BehaviorSubject<string> = new BehaviorSubject(null);


  constructor(
    private readonly http: HttpClient,
  ) {}

  public getAllLanguages(): Observable<LanguageDTO[]> {
    return this.http.get<LanguageDTO[]>(`http://localhost:3000/languages`);
  }

  public getSuppLanguages(): Observable<LanguageDTO[]> {
    return this.http.get<LanguageDTO[]>(`http://localhost:3000/languages/supported`);
  }

  public changeLanguagesSupportStatusByName(langsNames: string[]): Observable<LanguageDTO[]> {
    return this.http.put<LanguageDTO[]>(`http://localhost:3000/languages/supported`, langsNames);
  }

  public get displayLanguage$() {
    return this.appLanguage$.asObservable();
  }

  public changeLanguage(lang: string) {

    this.appLanguage$.next(lang);
  }

}
