import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { CreateArticleDTO } from '../../features/articles/models/create-article.dto';
import { EditArticleDTO } from '../../features/articles/models/edit-article.dto';
import { ShowArticleVersionDTO } from '../../features/articles/models/show-article-version.dto';
import { DetectedLanguageDTO } from '../../features/languages/models/detected-language.dto';

@Injectable()
export class ArticlesService {
  private readonly baseUrl: string = `http://localhost:3000/articles`;
  constructor(
    private readonly http: HttpClient,
  ) {

  }

  public getLatestArticlesInLang(lang: string): Observable<ShowArticleVersionDTO[]> {

    return this.http.get<ShowArticleVersionDTO[]>(`${this.baseUrl}/latest?lang=${lang}`);
  }

  public getOriginalArticle(id: string): Observable<ShowArticleVersionDTO> {
    return this.http.get<ShowArticleVersionDTO>(`${this.baseUrl}/${id}`);
  }

  public getAllArticles(lang: string): Observable<ShowArticleVersionDTO[]> {
    return this.http.get<ShowArticleVersionDTO[]>(`${this.baseUrl}?lang=${lang}`);
  }

  public getArticlesFromCategoryInLang(lang: string, category: string): Observable<ShowArticleVersionDTO[]> {

    return this.http.get<ShowArticleVersionDTO[]>(`${this.baseUrl}?lang=${lang}&category=${category}`);
  }

  public getArticleByIdInLang(articleId: string, lang: string): Observable<ShowArticleVersionDTO> {
    return this.http.get<ShowArticleVersionDTO>(`${this.baseUrl}/${articleId}?lang=${lang}`);
  }

  public createArticle(article: CreateArticleDTO): Observable<ShowArticleVersionDTO> {
    return this.http.post<ShowArticleVersionDTO>(`${this.baseUrl}`, article);
  }

  public updateArticle(articleId: string, updArticle: EditArticleDTO): Observable<ShowArticleVersionDTO> {
    return this.http.put<ShowArticleVersionDTO>(`${this.baseUrl}/${articleId}`, updArticle);
  }

  public getAllArticleVersions(articleId: string): Observable<ShowArticleVersionDTO[]> {
    return this.http.get<ShowArticleVersionDTO[]>(`${this.baseUrl}/${articleId}/versions`);
  }

  public revertArticle(articleId: string, version: { versionId: string }): Observable<ShowArticleVersionDTO> {
    return this.http.put<ShowArticleVersionDTO>(`${this.baseUrl}/${articleId}/versions`, version);
  }

  public detectArticleLang(content: { text: string }): Observable<DetectedLanguageDTO> {
    return this.http.post<DetectedLanguageDTO>(`http://localhost:3000/translations/detection`, content);
  }

  public deleteArticle(id: string): Observable<{msg: string}> {
    return this.http.delete<{msg: string}>(`${this.baseUrl}/${id}`);
  }
}
