import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { CommentDTO } from '../../features/comments/models/comment.dto';
import { DetectedLanguageDTO } from '../../features/languages/models/detected-language.dto';
import { ReplyDTO } from '../../features/replies/models/reply.dto';

@Injectable({
  providedIn: 'root'
})
export class RepliesService {

    private readonly baseURL = 'http://localhost:3000';

    constructor(
        private readonly http: HttpClient
    ) {}

    public getCommentsReplies(commentId: string, language: string): Observable<ReplyDTO[]> {
        return this.http.get<ReplyDTO[]>(`${this.baseURL}/comments/${commentId}/replies?lang=${language}`);
    }

    public editReply(commentId: string, replyId: string, newText: {content: string}): Observable<ReplyDTO> {
        return this.http.put<ReplyDTO>(`${this.baseURL}/comments/${commentId}/replies/${replyId}`, newText);
    }

    public createReply(commentId: string, newComment: {content: string, language: string}): Observable<ReplyDTO> {
        return this.http.post<ReplyDTO>(`${this.baseURL}/comments/${commentId}/replies`, newComment);
    }

    public detectReplyLang(content: { text: string }): Observable<DetectedLanguageDTO> {
        return this.http.post<DetectedLanguageDTO>(`http://localhost:3000/translations/detection`, content);
      }
}
