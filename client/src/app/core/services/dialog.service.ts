import { ComponentType } from '@angular/cdk/portal';
import { Injectable } from '@angular/core';
import { MatDialog, MatDialogConfig, MatDialogRef } from '@angular/material';



@Injectable()
export class DialogService {

  constructor(
    private readonly dialog: MatDialog,
  ) {
  }

  open(component: ComponentType<any>, data?: any): MatDialogRef<any> {
    const dialogConfig = new MatDialogConfig();

    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.data = data;
    dialogConfig.height = '600px';
    dialogConfig.width = '500px';

    const dialogRef = this.dialog.open(component,
        dialogConfig);

    return dialogRef;
  }
}
