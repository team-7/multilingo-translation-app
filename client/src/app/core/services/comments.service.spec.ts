import { CommentsService } from './comment.service';
import { UsersService } from './users.service';
import { AuthService } from './auth.service';
import { TestBed, async } from '@angular/core/testing';
import { of } from 'rxjs';
import { HttpClient } from '@angular/common/http';


describe('Users Service', () => {

    const http = {
        get() {
            return of();
        },
        post() {
            return of();
        },
        put() {
            return of();
        },
    };

    let getService: () => CommentsService;

    beforeEach(async (() => {

        jest.clearAllMocks();

        TestBed.configureTestingModule({
            imports: [],
            declarations: [],
            providers: [
                HttpClient,
                CommentsService
            ]
        }).overrideProvider(HttpClient, { useValue: http});

        getService = () => TestBed.get(CommentsService);

    }));

    it('should be defined', () => {
        // Arrange & Act & Assert
        expect(getService).toBeDefined();
    });

    describe('getArticleComments() should', () => {
        it('call http.get() once', () => {
            // Arrange
            const comments = [
                {
                    id: '1',
                    content: 'test1'
                },
                {
                   id: '2',
                   username: 'test2'
               },
            ];
            const articleId = '5';
            const spy = jest.spyOn(http, 'get').mockReturnValue(of(comments));
            // Act
            getService().getArticleComments(articleId,'en');
            // Assert
            expect(spy).toHaveBeenCalled();
            expect(spy).toHaveBeenCalledTimes(1);

        });

        describe('editComment() should', () => {
            it('call http.put() once', () => {
                // Arrange
                const articleId = '1';
                const comment = {
                    id: '1',
                    content: 'wood'
                };
                const spy = jest.spyOn(http, 'put').mockReturnValue(of(comment));
                // Act
                getService().editComment(articleId, comment.id, comment);
                // Assert
                expect(spy).toHaveBeenCalled();
                expect(spy).toHaveBeenCalledTimes(1);

            });
        });

        describe('createComment() should', () => {
            it('call http.post() once', () => {
                // Arrange
                const article = {
                    id: '1',
                    content: 'snow',
                };
                const spy = jest.spyOn(http, 'post').mockReturnValue(of(article));
                // Act
                getService().createComment(article.id, {content: 'pesho', language: 'en'});
                // Assert
                expect(spy).toHaveBeenCalled();
                expect(spy).toHaveBeenCalledTimes(1);
            });
        });

        describe('detectCommentLang() should', () => {
            it('call http.post() once', () => {
                // Arrange
                const spy = jest.spyOn(http, 'post').mockReturnValue(of('en'));
                // Act
                getService().detectCommentLang({text: 'pesho'});
                // Assert
                expect(spy).toHaveBeenCalled();
                expect(spy).toHaveBeenCalledTimes(1);
            });
        });
    });
});
