import { HttpClient } from '@angular/common/http';
import { TestBed } from '@angular/core/testing';
import { Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of } from 'rxjs';
import { UIService } from './ui.service';


describe('UI Service', () => {

  const http = {
    get() { return of(); },
  };

  let service: UIService;
  let router: Router;

  beforeEach(() => {
    jest.clearAllMocks();

    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
      ],
      providers: [
        UIService,
        HttpClient,
      ]
    })
    .overrideProvider(HttpClient, { useValue: http });

    service = TestBed.get(UIService);
    router = TestBed.get(Router);
  });

  it('should be defined', () => {
    // Arrange & Act & Assert
    expect(service).toBeDefined();
  });

  it('Initialize the subject with null', (done) => {

    service.UI$.subscribe(
      (ui) => {
        expect(ui).toBe(null);
      }
    );

    done();
  });

  describe('fireUISubject', () => {

    it('should call http get', () => {
      const language = 'en';

      const postSpy = jest.spyOn(http, 'get').mockReturnValue(of({}));

      service.fireUISubjectInLang(language);


      expect(postSpy).toBeCalledTimes(1);
      expect(postSpy).toBeCalledWith(`http://localhost:3000/ui-components?lang=${language}`);
    });

    it('should update uiSubject', (done) => {
      const language = 'en';

      const postSpy = jest.spyOn(http, 'get').mockReturnValue(of(language));

      service.fireUISubjectInLang(language);

      service.UI$.subscribe(
        (ui) => {
          expect(ui).toBe(language);

        }
        );

      done();
    });

    // it('should redirect if error', (done) => {
    //   const language = '';

    //   const spy = jest.spyOn(router, 'navigate');

    //   service.fireUISubjectInLang(language);

    //   service.UI$.subscribe(
    //     () => {},
    //     (error) => {
    //       expect(router.navigate).toHaveBeenCalledWith(['home']);
    //     },
    //   );
    //   done();
    // });

  });
});
