import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { LoggedUserDTO } from '../../features/users/models/logged-user.dto';
import { AuthService } from '../services/auth.service';
import { NotificationService } from '../services/notification.service';

@Injectable()
export class NotLoggedGuard {

  constructor(
    private readonly authService: AuthService,
    private readonly router: Router,
    private readonly notificationService: NotificationService
    ) {
  }

  public canActivate(): boolean {
    const user: LoggedUserDTO | undefined = this.authService.getUserDataIfAuthenticated();
    if (!user) {
        return true;
    }
    this.notificationService.error(`Sorry, you can't access this page!`);
    this.router.navigate(['/home']);
    return false;
  }

}
