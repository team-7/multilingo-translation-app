import { CommonModule } from '@angular/common';
import { NgModule, Optional, SkipSelf } from '@angular/core';
import { ToastrModule } from 'ngx-toastr';
import { ArticlesService } from './services/articles.service';
import { CategoriesService } from './services/categories.service';
import { DialogService } from './services/dialog.service';
import { NotificationService } from './services/notification.service';
import { RatingsService } from './services/ratings.service';
import { StorageService } from './services/storage.service';
import { TranslationsService } from './services/translation.service';
import { UsersService } from './services/users.service';


@NgModule({
  declarations: [],
  imports: [
    ToastrModule.forRoot({
      timeOut: 3000,
      positionClass: 'toast-bottom-right',
      preventDuplicates: true,
      countDuplicates: true,
    }),
    CommonModule
  ],
  providers: [
    // AuthService,
    StorageService,
    UsersService,
    NotificationService,
    ArticlesService,
    DialogService,
    TranslationsService,
    RatingsService,
    // LanguagesService,
    CategoriesService,
    // UIService,
  ]
})
export class CoreModule {
  constructor(
    @Optional() @SkipSelf() parent: CoreModule,
  ) {
    if (parent) {
      throw new Error('Core module is already initialized!');
    }
  }
}
