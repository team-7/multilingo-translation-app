import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UIService } from '../../core/services/ui.service';
import { UIComponentDTO } from '../../models/ui-component.dto';

@Component({
  selector: 'app-server-error-page',
  templateUrl: './server-error-page.component.html',
  styleUrls: ['./server-error-page.component.css']
})
export class ServerErrorPageComponent implements OnInit {

  public ui: UIComponentDTO;

  constructor(
    private readonly router: Router,
    private readonly uiService: UIService
  ) { }

  ngOnInit() {
    this.uiService.UI$.subscribe(
      (ui: UIComponentDTO) => {
        this.ui = ui;
      },
    );
  }

  public redirectToHomePage() {
    this.router.navigate(['/home']);
  }
}
