import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { LanguagesService } from '../../core/services/languages.service';
import { NotificationService } from '../../core/services/notification.service';
import { UIService } from '../../core/services/ui.service';
import { LanguageDTO } from '../../features/languages/models/language.dto';
import { UIComponentDTO } from '../../models/ui-component.dto';

@Component({
  selector: 'app-landing-page',
  templateUrl: './landing-page.component.html',
  styleUrls: ['./landing-page.component.css']
})
export class LandingPageComponent implements OnInit {

  public languages: LanguageDTO[];
  @Input()
  public ui: UIComponentDTO;
  public displayLang: string;

  constructor(
    private readonly router: Router,
    private readonly languagesService: LanguagesService,
    private readonly notificationsService: NotificationService,
    private readonly uiService: UIService,
    private readonly route: ActivatedRoute,
  ) { }

  ngOnInit() {

    this.languagesService.getSuppLanguages().subscribe(
      (data) => {
        this.languages = data;
      },
      (err) => {
        this.notificationsService.error(err.error.message);
        this.router.navigate(['/home']);
      }
    );

    this.languagesService.displayLanguage$.subscribe(
      (lang: string) => {
        this.displayLang = lang;
      }
    );
    this.uiService.UI$.subscribe(
      (ui: UIComponentDTO) => {
        this.ui = ui;
      },
    );
  }

  public goToHomePage() {
    this.router.navigate(['/home']);
  }
}
