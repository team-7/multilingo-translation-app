import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from '../../core/services/auth.service';
import { LanguagesService } from '../../core/services/languages.service';
import { NotificationService } from '../../core/services/notification.service';
import { UIService } from '../../core/services/ui.service';
import { UIComponentDTO } from '../../models/ui-component.dto';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  public hidePassword = true;
  public loginForm: FormGroup;

  @Input()
  public displayLang: string;

  @Input()
  public ui: UIComponentDTO;

  constructor(
    private readonly authService: AuthService,
    private readonly languagesService: LanguagesService,
    private readonly uiService: UIService,
    private readonly formBuilder: FormBuilder,
    private readonly router: Router,
    private readonly notificationService: NotificationService
  ) { }

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      username: ['', Validators.compose([Validators.required, Validators.minLength(4)])],
      password: ['', [Validators.required]],
      keepMeLoggedIn: [false],
    });
  }

  public login() {
    this.authService.login(this.loginForm.value)
    .subscribe(
      ({token}) => {
        const user = this.authService.getDecodedAccessToken(token);
        if (user.prefLang !== this.displayLang) {
          this.uiService.fireUISubjectInLang(user.prefLang).subscribe(_ => {
            this.languagesService.changeLanguage(user.prefLang);
          });
        }

        this.notificationService.success(`Login successful!`);
        this.router.navigate(['/home']);
      },
      (err) => this.notificationService.error(`${err.error.message}`),
    );
  }

}
